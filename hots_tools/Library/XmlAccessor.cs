﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;

namespace HotsTools
{
    public class XmlAccessor
    {
        public static object Load(Type classType, string filePath)
        {
            var serializer = new DataContractSerializer(classType);
            using (var reader = XmlReader.Create(filePath))
            {
                var obj = serializer.ReadObject(reader);
                return obj;
            }
        }

        public static void Save(Type classType, string filePath, object obj)
        {
            var serializer = new DataContractSerializer(classType);
            var settings = new XmlWriterSettings();
            settings.Encoding = new System.Text.UTF8Encoding(false);
            using (var writer = XmlWriter.Create(filePath, settings))
            {
                serializer.WriteObject(writer, obj);
            }
        }
    }
}
