include version_hots_talent_description.mk

MSBUILD = /c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/2017/Community/MSBuild/15.0/Bin/MSBuild.exe
SOLUTION_DIR = hots_talent_description/hots_talent_description
SLN_FILE = $(SOLUTION_DIR)/HotsTalentDescription.sln
BIN_DIR = $(SOLUTION_DIR)/HotsTalentDescription/bin/Release
SCRIPT_DIR = hots_talent_description/script
TOP_DIR = hots_talent_description

DIST_DIR = $(PACKAGE_DIR)
DIST_FILES = $(BIN_DIR)/HotsTalentDescription.exe $(BIN_DIR)/*.dll $(TOP_DIR)/README.txt $(TOP_DIR)/LICENSE.txt $(TOP_DIR)/ChangeLog.txt

DIST_SCRIPT_FILES = $(SCRIPT_DIR)/update.vbs $(SCRIPT_DIR)/download_data.py $(SCRIPT_DIR)/get_hero_links.py $(SCRIPT_DIR)/html_to_hero_yaml.py
DIST_SCRIPT_DIR = $(PACKAGE_DIR)/script


all : clean
	mkdir -p $(DIST_DIR)/data/img
	mkdir -p $(DIST_DIR)/tessdata
	mkdir -p $(DIST_DIR)/x86
	mkdir -p $(DIST_DIR)/ja-JP
	mkdir -p $(DIST_SCRIPT_DIR)
	$(MSBUILD) $(SLN_FILE) -t:build -p:Configuration=Release
	cp $(DIST_FILES) $(DIST_DIR)
	cp $(BIN_DIR)/tessdata/eng.traineddata $(DIST_DIR)/tessdata/
	cp $(BIN_DIR)/x86/*.dll $(DIST_DIR)/x86/
	cp $(BIN_DIR)/ja-JP/*.dll $(DIST_DIR)/ja-JP/
	cp $(DIST_SCRIPT_FILES) $(DIST_SCRIPT_DIR)
	zip -r $(DIST_DIR).zip $(DIST_DIR)

clean :
	$(RM) -rf $(DIST_DIR)
