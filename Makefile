all :

clean :

upload :
	$(RM) html/*~
	rsync -avz -e ssh --delete html/* hyakuren-soft@hyakuren-soft.sakura.ne.jp:/home/hyakuren-soft/www/hots_tools/

dist :
	$(MAKE) -f dist_hots_replay_chat.mk all
	$(MAKE) -f dist_hots_draft_viewer.mk all
	$(MAKE) -f dist_hots_talent_description.mk all

distclean :
	$(MAKE) -f dist_hots_replay_chat.mk clean
	$(MAKE) -f dist_hots_draft_viewer.mk clean
	$(MAKE) -f dist_hots_talent_description.mk clean
