hots_memo README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/bin/initialize_hots_memo_db development.ini

- $VENV/bin/pserve development.ini

