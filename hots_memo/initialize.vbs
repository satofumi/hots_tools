Dim wshShell
Set wshShell = CreateObject("WScript.Shell")

Dim wshEnv
Set wshEnv = wshShell.Environment("Process")

wshEnv.Item("PATH") = wshShell.ExpandEnvironmentStrings("C:\Python27;C:\Python27\Scripts") & ";" & wshShell.ExpandEnvironmentStrings("%PATH%")

wshShell.Run "C:\Python27\python -m pip install virtualenvwrapper-win", 1, True
Dim fso
Set fso = CreateObject("Scripting.FileSystemObject")

if fso.FolderExists("%UserProfile%\Envs\hots_memo") = False Then
wshShell.Run "mkvirtualenv hots_memo", 1, True
End If

wshShell.Run "workon hots_memo & python setup.py develop & initialize_hots_memo_db production.ini", 1, True
