Dim wshShell
Set wshShell = CreateObject("WScript.Shell")

Dim wshEnv
Set wshEnv = wshShell.Environment("Process")

wshEnv.Item("PATH") = wshShell.ExpandEnvironmentStrings("C:\Python27;C:\Python27\Scripts;C:\Program Files\Git\bin") & ";" & wshShell.ExpandEnvironmentStrings("%PATH%")

Dim netstat, re
Set netstat = wshShell.Exec("NETSTAT -an")
Set re = new regexp
re.Pattern = "TCP.*0.0.0.0:6127.*LISTENING"

If Not re.Test(netstat.StdOut.ReadAll()) Then
wshShell.Run "workon hots_memo & pserve production.ini --reload", 0, False
End If

wshShell.Run "http://localhost:6128", 1
