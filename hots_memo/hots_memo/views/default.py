from pyramid.response import Response
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError
from pyramid.httpexceptions import HTTPFound

from ..models import Memo


@view_config(route_name='home', renderer='../templates/memo.jinja2')
def home(request):
    return memo(request)

@view_config(route_name='memo',
             renderer='../templates/memo.jinja2')
def memo(request):
    memos = request.dbsession.query(Memo)
    return {
        'memos': memos,
    }

@view_config(route_name='add_memo')
def add_memo(request):
    hero = request.POST.get('hero', None)
    field = request.POST.get('field', None)
    message = request.POST.get('message', None)
    memo = Memo(hero=hero, field=field, message=message)

    request.dbsession.add(memo)

    url = request.route_url('home')
    return HTTPFound(location=url)
