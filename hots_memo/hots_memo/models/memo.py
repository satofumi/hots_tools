from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime,
)

from .meta import Base
from datetime import datetime


class Memo(Base):
    __tablename__ = 'memos'
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=datetime.utcnow)
    hero = Column(Text)
    field = Column(Text)
    message = Column(Text)
