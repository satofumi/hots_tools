include version_hots_draft_viewer.mk

DIST_DIR = $(PACKAGE_DIR)
TOP_DIR = hots_draft_viewer
WEB_DIR = hots_draft_viewer
TEMPLATES_DIR = $(WEB_DIR)/templates
STATIC_DIR = $(WEB_DIR)/static
ICONS_DIR = $(STATIC_DIR)/icons

DIST_FILES = \
	$(TOP_DIR)/README.md \
	$(TOP_DIR)/LICENSE.txt \
	$(TOP_DIR)/production.ini \
	$(TOP_DIR)/setup.py \
	$(TOP_DIR)/*.vbs \
	$(TOP_DIR)/pywin32.pth \

WEB_FILES = \
	$(TOP_DIR)/$(WEB_DIR)/*.py \

STATIC_FILES = \
	$(TOP_DIR)/$(STATIC_DIR)/theme.css \

ICONS_FILES = \
	$(TOP_DIR)/$(STATIC_DIR)/icons/*.png \


all : clean
	mkdir -p $(DIST_DIR)
	mkdir -p $(DIST_DIR)/$(WEB_DIR)
	mkdir -p $(DIST_DIR)/$(SCRIPTS_DIR)
	mkdir -p $(DIST_DIR)/$(STATIC_DIR)
	mkdir -p $(DIST_DIR)/$(ICONS_DIR)
	mkdir -p $(DIST_DIR)/$(VIEWS_DIR)
	cp $(DIST_FILES) $(DIST_DIR)
	cp $(WEB_FILES) $(DIST_DIR)/$(WEB_DIR)
	cp $(STATIC_FILES) $(DIST_DIR)/$(STATIC_DIR)
	cp $(ICONS_FILES) $(DIST_DIR)/$(ICONS_DIR)
	cp -R $(WEB_DIR)/heroprotocol $(DIST_DIR)/
	zip -r $(DIST_DIR).zip $(DIST_DIR)

clean :
	$(RM) -rf $(DIST_DIR)
