#* -*- coding: utf-8 -*-
import sys
import os
import re
import requests
import urllib
import html_to_hero_yaml


headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

images = set()
link_file = sys.argv[1] if len(sys.argv) >= 2 else 'hero_links.csv'
with open(link_file) as f:
    links = f.readlines()
    for line in links:
        tokens = line.strip().split(',')
        name = tokens[0]
        link = tokens[1]
        print(link)

        result = re.match('(http.+?//.+?)/', link)

        page = requests.get(link, headers=headers).content.decode()
        yaml_text, image_links = html_to_hero_yaml.yaml(page, link, name)

        # save yaml data
        with open('../data/' + name + '.yaml', 'w') as f:
            f.write(yaml_text)

        for image_link in image_links:
            if len(image_link) > 0:
                images.add(image_link)

# download image files
for link in images:
    if len(link) == 0:
        continue

    file_path = '../data/img/' + link.replace('/', '_').replace(':', '=').replace('?', '!') + '.png'
    if os.path.exists(file_path):
        continue

    print('  image: ' + link)
    page = requests.get(link, headers=headers).content
    result = re.match('(http.+?//.+?)/', link)
    url_base = result.group(1) if result else ''

    with open(file_path, 'wb') as f:
        f.write(page)
