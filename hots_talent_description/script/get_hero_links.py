import sys
import requests
import re


def links():
    url_base = 'https://wikiwiki.jp'
    url = url_base + '/hots-j/Hero'

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    page = requests.get(url, headers=headers)

    tables = re.findall('<table.+?</table>', page.content.decode())
    links = set()
    for table_text in tables:
        result = re.findall('<a href=".+?"\s+title=".+?">', table_text)
        for link in result:
            if link.find('alt=') < 0:
                links.add(link)

    output = ''
    for link in links:
        result = re.search('href="(.+?)"\s+title="(.+?)"', link)
        output += result.group(2) + "," + url_base + result.group(1) + "\n"

    return output


if __name__ == '__main__':
    output_file = sys.argv[1] if len(sys.argv) >= 2 else 'hero_links.csv'
    with open(output_file, 'w') as f:
        f.write(links())
