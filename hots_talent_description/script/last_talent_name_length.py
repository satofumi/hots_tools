import yaml
import glob

min_length = 128
min_name = ''

for yaml_file in glob.glob('../data/*.yaml'):
    with open(yaml_file) as f:
        data = yaml.load(f)
        for level,talents in data['talents'].items():
            last_index = len(talents) - 1
            name = talents[last_index]['talent'][1]
            length = len(name)
            if length < min_length:
                min_length = length
                min_name = name

print min_name
print min_length
