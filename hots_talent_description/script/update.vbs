Dim wshShell
Set wshShell = CreateObject("WScript.Shell")

Dim wshEnv
Set wshEnv = wshShell.Environment("Process")

wshEnv.Item("PATH") = wshShell.ExpandEnvironmentStrings("C:\Python27;C:\Python27\Scripts") & ";" & wshShell.ExpandEnvironmentStrings("%PATH%")

wshShell.Run "C:\Python27\python -m pip install virtualenvwrapper-win", 1, True

Dim fso
Set fso = CreateObject("Scripting.FileSystemObject")

If fso.FolderExists("%UserProfile%\Envs\hots") = False Then
wshShell.Run "mkvirtualenv hots", 1, True
wshShell.Run "workon hots & C:\Python27\python -m pip install requests", 1, True
End If

wshShell.Run "workon hots & C:\Python27\python get_hero_links.py", 1, True
wshShell.Run "workon hots & C:\Python27\python download_data.py", 1, True
