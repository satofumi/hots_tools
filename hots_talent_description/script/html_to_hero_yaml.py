#* -*- coding: utf-8 -*-
import sys
import os
import re


def parse_src_path(text):
    result = re.search('src="(.+?)"', text)
    path = result.group(1) if result else ''
    return path.replace('&amp;', '&').replace('%2F', '/')


def parse_ability_tr(tr_text):
    result = re.findall('<t[dh].*?>(.*?)</t[dh]>', tr_text)
    type_image = parse_src_path(result[0])
    icon_image = parse_src_path(result[1])

    if len(result) == 2 or len(result) == 3 or len(result) == 4:
        # Fenix W skill(2)
        # Chen W skill(4)
        # Tychus R skill(3)
        return ['', '', '', '']

    name_tokens = result[2].split('<br class="spacer" />')
    name = name_tokens[1] + ' (' + name_tokens[0] + ')'
    description = result[4]

    return [icon_image, name, type_image, description]


def yaml(html_text, html_address, hero_name = None):
    # parse heroic ability
    h2_text = re.search('<h2 id="h2_content_1_1">(.+?)<h2', html_text, re.DOTALL)
    basic = []
    heroic = []
    index = 0
    for table_text in re.findall('<table.+?>(.+?)</table>', h2_text.group(1)):
        if index == 0:
            for tr_text in re.findall('<tr>(.+?)</tr>', table_text):
                basic.append(parse_ability_tr(tr_text))

        if index == 1:
            for tr_text in re.findall('<tr>(.+?)</tr>', table_text):
                heroic.append(parse_ability_tr(tr_text))
        index += 1
    if hero_name == 'Tracer' and len(heroic) == 1:
        heroic.pop(0)
    elif hero_name == 'Alarak':
        heroic.pop(1)

    # parse talents
    table_text = re.search('<h2 id="h2_content_1_2">.+?</table>', html_text, re.DOTALL)

    output = 'link: "' + html_address + '"\n'
    output += 'talents:\n'
    image_links = []
    level = 1
    for tr_text in re.findall('<tr>.+?</tr>', table_text.group(0)):
        level_line = re.search('<strong>Lv(\d+)</strong>', tr_text)
        if level_line:
            level = level_line.group(1)
            output += '  ' + level + ':\n'

        else:
            additional_talent = False
            # icon, name, type, description
            result = re.findall('<t[dh].*?>(.*?)</t[dh]>', tr_text)

            # Deathwing
            if len(result) == 1:
                result = ['', '', '', result[0]]
                additional_talent = True

            icon_image = parse_src_path(result[0])
            image_links.append(icon_image)

            name = result[1]

            type_image = parse_src_path(result[2])
            image_links.append(type_image)

            if len(result) == 3:
                description = previous_description
            else:
                description = result[3]
                previous_description = description

            # replace The Lost Vikings description
            if hero_name == 'The Lost Vikings':
                if name == 'Viking Bribery':
                    description += '\\n' + basic[4][3]
                elif name == 'Spin To Win!':
                    description += '\\n' + basic[0][3]
                elif name == 'Norse Force!':
                    description += '\\n' + basic[1][3]
                elif name == 'Jump!':
                    description += '\\n' + basic[2][3]
                elif name == 'Nordic Attack Squad':
                    description += '\\n' + basic[3][3]

            # replace heroic description
            if level == '8' or level == '10':
                if len(heroic) > 0:
                    description = heroic[0][3]
                    heroic.pop(0)

            description = description.replace('<br class="spacer" />', '\\n')
            description = re.sub('<span.+?Quest</span>', 'Quest', description, flags=re.IGNORECASE)
            description = re.sub('<span.+?Reward</span>', 'Reward', description, flags=re.IGNORECASE)
            description = re.sub('<span.+?Active</span>', 'Active', description, flags=re.IGNORECASE)
            description = re.sub('<span.+?Passive</span>', 'Passive', description, flags=re.IGNORECASE)
            description = re.sub('<span.+?>(.+?)</span>', r'\1', description, flags=re.IGNORECASE)
            description = re.sub('<img src.+?Passive.png.+?/>', 'Passive', description, flags=re.IGNORECASE)
            description = re.sub('<img src.+?Active.png.+?/>', 'Active', description, flags=re.IGNORECASE)
            description = description.replace('&quot;', '\\"')
            description = description.replace('<a href="/hots-j/Gall" title="Gall">Gall</a>', 'Gall')
            description = description.replace('<a href="/hots-j/Cho" title="Cho">Cho</a>', 'Cho')
            description = description.strip()

            if additional_talent:
                output = output[:-3]
                output += '\\n\\n' + description + '"]\n'
            else:
                output += '    - talent: ["' + icon_image + '", "' + name + '", "' + type_image + '", "' + description + '"]\n'

    return output, image_links


if __name__ == '__main__':
    input_file = sys.argv[1]
    with open(input_file) as f:
        html_text = f.read()
        yaml_text, image_links = yaml(html_text, input_file)
        print(yaml_text)
