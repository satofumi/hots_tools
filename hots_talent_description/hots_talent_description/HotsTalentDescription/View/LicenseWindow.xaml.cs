﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HotsTalentDescription.View
{
    /// <summary>
    /// LicenseWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class LicenseWindow : Window
    {
        private bool closeCancel = true;


        public LicenseWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = closeCancel;
            Visibility = Visibility.Collapsed;
        }

        public void Quit()
        {
            closeCancel = false;
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var sr = new StreamReader("LICENSE.txt"))
                {
                    LicenseText.Text = sr.ReadToEnd();
                }
            }
            catch (Exception)
            {
                // do nothing
            }
        }
    }
}
