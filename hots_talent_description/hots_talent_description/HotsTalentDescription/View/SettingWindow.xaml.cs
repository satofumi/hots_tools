﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Runtime.InteropServices;

namespace HotsTalentDescription
{
    /// <summary>
    /// SettingWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class SettingWindow : Window
    {
        [DllImport("gdi32")]
        static extern int DeleteObject(IntPtr o);


        private bool closeCancel = true;
        private Setting setting;


        public SettingWindow()
        {
            InitializeComponent();
        }

        public void Apply(Setting setting)
        {
            this.setting = setting;

            var area = setting.TalentNameArea;
            UpDownX.SetValue(area.Left);
            UpDownY.SetValue(area.Top);
            UpDownW.SetValue(area.Width);
            UpDownH.SetValue(area.Height);

            UseAutoTalentCheckBox.IsChecked = setting.UseAutoTalent;
            AutoCloseCheckBox.IsChecked = setting.AutoClose;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = closeCancel;
            Visibility = Visibility.Collapsed;
        }

        public void Quit()
        {
            closeCancel = false;
            Close();
        }

        public void ApplyCaptureData(Bitmap image, string result)
        {
            if (Visibility == Visibility.Visible)
            {
                Dispatcher.Invoke(new Action(() =>
                {
                    CaptureImage.Source = LoadBitmap(image);
                    ResultLabel.Content = result;
                }));
            }
        }

        public BitmapSource LoadBitmap(System.Drawing.Bitmap source)
        {
            IntPtr ip = source.GetHbitmap();
            BitmapSource bs = null;
            try
            {
                bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ip, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }
            finally
            {
                DeleteObject(ip);
            }

            return bs;
        }

        private void UpdateRectangle()
        {
            setting.TalentNameArea = new Rectangle(UpDownX.Value, UpDownY.Value, UpDownW.Value, UpDownH.Value);
        }

        private void UpDownX_ValueChanged(object sender, EventArgs e)
        {
            UpdateRectangle();
        }

        private void UpDownY_ValueChanged(object sender, EventArgs e)
        {
            UpdateRectangle();
        }

        private void UpDownW_ValueChanged(object sender, EventArgs e)
        {
            UpdateRectangle();
        }

        private void UpDownH_ValueChanged(object sender, EventArgs e)
        {
            UpdateRectangle();
        }

        private void UseAutoTalentCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            setting.UseAutoTalent = true;
            SettingVisible = true;
        }

        private void UseAutoTalentCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            setting.UseAutoTalent = false;
            SettingVisible = false;
        }

        private void AutoCloseCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            setting.AutoClose = true;
        }

        private void AutoCloseCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            setting.AutoClose = false;
        }

        private bool SettingVisible
        {
            set
            {
                AutoCloseCheckBox.IsEnabled = value;
                AdjustGroupBox.IsEnabled = value;
                CheckGroupBox.IsEnabled = value;
            }
        }
    }
}
