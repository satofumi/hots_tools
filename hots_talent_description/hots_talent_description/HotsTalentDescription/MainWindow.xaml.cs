﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.ComponentModel;
using System.Drawing;
using HotsTalentDescription.Control;
using HotsTalentDescription.View;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using HotsTools;

namespace HotsTalentDescription
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string ProjectName = "HotsTools";
        private const string ApplicationName = "TalentDescription";
        private static readonly string PersonalDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), ProjectName, ApplicationName);
        private static readonly string SettingFilePath = Path.Combine(PersonalDataFolder, "setting.xml");

        private Setting setting = new Setting();
        private string dataPath = "..\\..\\..\\..\\data";
        private Dictionary<string, string> namePathIndexes = new Dictionary<string, string>();
        private Dictionary<string, Hero> heroesData = new Dictionary<string, Hero>();
        private List<TabItem> tabItems;
        private List<Grid> gridItems;

        private SettingWindow settingWindow = new SettingWindow();
        private LicenseWindow licenseWindow = new LicenseWindow();

        private BackgroundWorker captureWorker = new BackgroundWorker();
        private bool isControlManually;
        private List<string> lastTalents;
        private const int LevenshteinLengthThreshold = 6;

        private string currentHeroWikiAddress;


        public MainWindow()
        {
            InitializeComponent();

            tabItems = new List<TabItem>() { Tab0, Tab1, Tab2, Tab3, Tab4, Tab5, Tab6 };
            gridItems = new List<Grid>() { Grid0, Grid1, Grid2, Grid3, Grid4, Grid5, Grid6 };

            FormVisible = false;
            this.SizeToContent = SizeToContent.Height;

            if (!File.Exists(SettingFilePath))
            {
                Directory.CreateDirectory(PersonalDataFolder);
                setting.Left = 5;
                setting.Top = 5;
                SaveSetting();
            }
            LoadSetting();
        }

        private void LoadSetting()
        {
            try
            {
                setting = XmlAccessor.Load(typeof(Setting), SettingFilePath) as Setting;
                this.Left = setting.Left;
                this.Top = setting.Top;

                settingWindow.Apply(setting);
            }
            catch (Exception)
            {
                // do nothing.
            }
        }

        private void SaveSetting()
        {
            try
            {
                XmlAccessor.Save(typeof(Setting), SettingFilePath, setting);
            }
            catch (Exception)
            {
                // do nothing.
            }
        }

        private bool FormVisible
        {
            set
            {
                try
                {
                    Dispatcher.Invoke(new Action(() =>
                    {

                        ShowButton.Visibility = !value ? Visibility.Visible : Visibility.Collapsed;
                        HideButton.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                        TabPanel.Visibility = value ? Visibility.Visible : Visibility.Collapsed;

                    }));
                }
                catch (Exception)
                {
                    // do nothing
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var wndHelper = new WindowInteropHelper(this);
            int exStyle = (int)WindowStyles.GetWindowLong(wndHelper.Handle, (int)WindowStyles.GetWindowLongFields.GWL_EXSTYLE);
            exStyle |= (int)WindowStyles.ExtendedWindowStyles.WS_EX_TOOLWINDOW;
            WindowStyles.SetWindowLong(wndHelper.Handle, (int)WindowStyles.GetWindowLongFields.GWL_EXSTYLE, (IntPtr)exStyle);

            if (!Directory.Exists(dataPath))
            {
                dataPath = "data/";
            }

            var directory = new DirectoryInfo(dataPath);
            var files = directory.GetFiles("*.yaml");

            foreach (var fileInfo in files)
            {
                string path = fileInfo.FullName;
                string name = Path.GetFileNameWithoutExtension(path);
                namePathIndexes[name] = path;
                HeroNamesComboBox.Items.Add(name);
            }

            if (HeroNamesComboBox.Items.Count > 0)
            {
                HeroNamesComboBox.SelectedItem = setting.SelectedHeroName;
                if (HeroNamesComboBox.SelectedIndex < 0)
                {
                    HeroNamesComboBox.SelectedIndex = 0;
                }
            }

            captureWorker.WorkerSupportsCancellation = true;
            captureWorker.WorkerReportsProgress = true;
            captureWorker.DoWork += CaptureWorker_DoWork;
            captureWorker.RunWorkerAsync();
        }

        private void CaptureWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!captureWorker.CancellationPending)
            {
                if (setting.UseAutoTalent)
                {
                    CaptureTalentName(setting.TalentNameArea);
                }
                System.Threading.Thread.Sleep(25);
            }
            e.Cancel = true;
        }

        private void CaptureTalentName(Rectangle rectangle)
        {
            if ((setting.TalentNameArea.Width == 0) || (setting.TalentNameArea.Height == 0))
            {
                return;
            }

            var image = Capture.CaptureImage(setting.TalentNameArea);
            var text = Capture.ParseText(image).Trim();

            if (settingWindow.Visibility == Visibility.Visible)
            {
                settingWindow.ApplyCaptureData(image, text);
            }

            if (isControlManually)
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(text) || (text == "Empty page!!") || (text == "Co-op") || (text.Length < 4))
            {
                if (setting.AutoClose)
                {
                    FormVisible = false;
                }
                return;
            }

            int index = 0;
            int minLength = int.MaxValue;
            int minIndex = 0;
            foreach (var talent in lastTalents)
            {
                if (talent == text)
                {
                    ChangeTab(index);
                    FormVisible = true;
                    return;
                }

                int lengthValue = CalcLevenshteinDistance(talent, text);
                if (lengthValue < minLength)
                {
                    minLength = lengthValue;
                    minIndex = index;
                }
                ++index;
            }

            if (minLength <= LevenshteinLengthThreshold)
            {
                ChangeTab(minIndex);
                FormVisible = true;
            }
            else
            {
                if (setting.AutoClose)
                {
                    FormVisible = false;
                }
            }
        }

        private static int CalcLevenshteinDistance(string a, string b)
        {
            if (String.IsNullOrEmpty(a) || String.IsNullOrEmpty(b))
            {
                return 0;
            }

            int lengthA = a.Length;
            int lengthB = b.Length;
            var distances = new int[lengthA + 1, lengthB + 1];
            for (int i = 0; i <= lengthA; distances[i, 0] = i++) ;
            for (int j = 0; j <= lengthB; distances[0, j] = j++) ;

            for (int i = 1; i <= lengthA; i++)
            {
                for (int j = 1; j <= lengthB; j++)
                {
                    int cost = b[j - 1] == a[i - 1] ? 0 : 1;
                    distances[i, j] = Math.Min(Math.Min(distances[i - 1, j] + 1, distances[i, j - 1] + 1), distances[i - 1, j - 1] + cost);
                }
            }
            return distances[lengthA, lengthB];
        }

        private void ChangeTab(int index)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                TabPanel.SelectedIndex = index;
            }));
        }

        private void ShowButton_Click(object sender, RoutedEventArgs e)
        {
            FormVisible = true;
            isControlManually = true;
        }

        private void HideButton_Click(object sender, RoutedEventArgs e)
        {
            FormVisible = false;
            isControlManually = true;
        }

        private void HeroNamesComboBox_DropDownClosed(object sender, EventArgs e)
        {
            isControlManually = false;
        }

        private Hero LoadHeroData(string name)
        {
            var hero = new Hero();
            hero.LoadYamlFile(namePathIndexes[name]);
            heroesData.Add(name, hero);
            return hero;
        }

        private void HeroNamesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string name = HeroNamesComboBox.SelectedItem.ToString();
            Hero hero = heroesData.ContainsKey(name) ? heroesData[name] : LoadHeroData(name);
            currentHeroWikiAddress = hero.WikiPageAddress;

            isControlManually = false;
            lastTalents = new List<string>();
            int talentCount = 7;
            for (int i = 0; i < talentCount; ++i)
            {
                var talents = hero.Talents(i);
                tabItems[i].Header = talents.Level;

                var grid = gridItems[i];
                grid.Children.Clear();
                grid.ColumnDefinitions.Clear();
                grid.RowDefinitions.Clear();

                var column0 = new ColumnDefinition();
                column0.Width = new GridLength(56);
                var column1 = new ColumnDefinition();
                grid.ColumnDefinitions.Add(column0);
                grid.ColumnDefinitions.Add(column1);

                for (int k = 0; k < talents.Talents.Count; ++k)
                {
                    var row = new RowDefinition();
                    row.Height = GridLength.Auto;
                    grid.RowDefinitions.Add(row);

                    // Image
                    string imageFile = talents.Talents[k].IconImagePath;
                    if (!string.IsNullOrEmpty(imageFile))
                    {
                        var imagePath = System.IO.Path.Combine(Environment.CurrentDirectory, dataPath, "img", imageFile.Replace('/', '_').Replace(':', '=').Replace('?', '!').Replace("%", "%25") + ".png");
                        var uri = new Uri("file://" + imagePath);
                        var image = new System.Windows.Controls.Image();
                        image.VerticalAlignment = VerticalAlignment.Top;
                        image.Margin = new Thickness(4, 6, 4, 4);
                        image.Source = new BitmapImage(uri);
                        Grid.SetRow(image, k);
                        Grid.SetColumn(image, 0);
                        grid.Children.Add(image);
                    }

                    // Description
                    var description = new TextBlock();
                    description.FontSize = 14;
                    description.Foreground = System.Windows.Media.Brushes.LightCyan;
                    description.TextWrapping = TextWrapping.WrapWithOverflow;
                    description.HorizontalAlignment = HorizontalAlignment.Stretch;
                    description.Margin = new Thickness(4, 4, 6, 6);

                    string talentName = talents.Talents[k].Name;
                    description.Inlines.Add(new Bold(new Run(talentName)));
                    description.Inlines.Add(Environment.NewLine);
                    description.Inlines.Add(talents.Talents[k].Description);

                    Grid.SetRow(description, k);
                    Grid.SetColumn(description, 1);
                    grid.Children.Add(description);

                    if (k == (talents.Talents.Count - 1))
                    {
                        lastTalents.Add(talentName);
                    }
                }
            }

            if (setting.SelectedHeroName != name)
            {
                setting.SelectedHeroName = name;
                SaveSetting();
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            captureWorker.CancelAsync();
            SaveSetting();
            settingWindow.Quit();
            licenseWindow.Quit();
            Close();
        }

        private void DragSpaceImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
                setting.Left = this.Left;
                setting.Top = this.Top;
                SaveSetting();
            }
        }

        private void Setting_Click(object sender, RoutedEventArgs e)
        {
            settingWindow.Activate();
            settingWindow.Show();
        }

        private void License_Click(object sender, RoutedEventArgs e)
        {
            licenseWindow.Activate();
            licenseWindow.Show();
        }

        private void OpenWikiPage_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(currentHeroWikiAddress))
            {
                return;
            }

            System.Diagnostics.Process.Start(currentHeroWikiAddress);
        }
    }
}
