﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace HotsTalentDescription
{
    public class Setting
    {
        public double Left { get; set; }
        public double Top { get; set; }

        public bool UseAutoTalent { get; set; }
        public bool AutoClose { get; set; }
        public Rectangle TalentNameArea { get; set; }

        public string SelectedHeroName { get; set; }


        public Setting()
        {
            Left = 5;
            Top = 5;

            UseAutoTalent = true;
            AutoClose = true;
            TalentNameArea = new Rectangle(95, 833, 300, 28);
        }
    }
}
