﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace HotsTalentDescription
{
    public class Hero
    {
        public class Talent
        {
            public class EachTalent
            {
                public string IconImagePath { get; set; }
                public string Name { get; set; }
                public string TypeImagePath { get; set; }
                public string Description { get; set; }
            }

            public List<LevelData> Talents = new List<LevelData>();

            public class LevelData
            {
                public int Level { get; set; }
                public List<EachTalent> Talents = new List<EachTalent>();

                public LevelData(int level, List<EachTalent> talents)
                {
                    Level = level;
                    Talents = talents;
                }
            }

            public LevelData Index(int index)
            {
                return Talents[index];
            }
        }


        private Talent talentData = new Talent();


        private class YamlHero
        {
            public string Link { get; set; }
            public Dictionary<int, List<LevelsTalent>> Talents { get; set; }

            public class LevelsTalent
            {
                public List<string> Talent { get; set; }
            }
        }

        private YamlHero Deserialize(string filePath)
        {
            var reader = new StreamReader(filePath, Encoding.GetEncoding("shift_jis"));
            string text = reader.ReadToEnd();
            var input = new StringReader(text);
            var deserializer = new DeserializerBuilder().WithNamingConvention(new UnderscoredNamingConvention()).Build();
            return deserializer.Deserialize<YamlHero>(input);
        }

        public void LoadYamlFile(string path)
        {
            var yamlHero = Deserialize(path);
            WikiPageAddress = yamlHero.Link;

            talentData.Talents.Clear();

            foreach (var item in yamlHero.Talents)
            {
                var level = item.Key;
                var levelsTalents = item.Value;
                var eachTalents = new List<Talent.EachTalent>();
                foreach (var talent in levelsTalents)
                {
                    var talentData = new Talent.EachTalent();
                    talentData.IconImagePath = talent.Talent[0];
                    talentData.Name = talent.Talent[1];
                    talentData.TypeImagePath = talent.Talent[2];
                    talentData.Description = talent.Talent[3];
                    eachTalents.Add(talentData);
                }
                talentData.Talents.Add(new Talent.LevelData(level, eachTalents));
            }
        }

        public Talent.LevelData Talents(int index)
        {
            return talentData.Index(index);
        }

        public string WikiPageAddress { get; private set; }
    }
}
