﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace HotsTalentDescription.Control
{
    public static class Capture
    {
        public static string ParseText(Bitmap captureImage)
        {
            using (var tesseract = new Tesseract.TesseractEngine("tessdata", "eng"))
            {
                var page = tesseract.Process(captureImage);
                return page.GetText();
            }
        }

        public static Bitmap CaptureImage(Rectangle rectangle)
        {
            var screenBmp = new Bitmap(rectangle.Width, rectangle.Height, PixelFormat.Format32bppArgb);
            using (var bmpGraphics = Graphics.FromImage(screenBmp))
            {
                bmpGraphics.CopyFromScreen(rectangle.X, rectangle.Y, 0, 0, screenBmp.Size);
                return screenBmp;
            }
        }
    }
}
