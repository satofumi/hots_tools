﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace HotsTalentDescription
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        [STAThread]
        public static void Main()
        {
            const string uniqueName = "HotsTalentDescription";
            bool created;
            using (var mutex = new System.Threading.Mutex(true, uniqueName, out created))
            {
                if (!created)
                {
                    string message = "Application already running.";
                    MessageBox.Show(message, "Critical", MessageBoxButton.OK, MessageBoxImage.Stop);
                    Current.Shutdown();
                }
                else
                {
                    App app = new App();
                    app.InitializeComponent();
                    app.Run();
                }
            }
        }
    }
}
