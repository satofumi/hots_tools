# HotS Talent Description

Heroes of the Storm のタレント情報を表示するツールです。


## 使い方

 - 起動: HotsTalentDescription.exe を実行します。
 - 終了: ボタンを右クリックして Quit を選択します。

 - 表示位置の移動: 左側の白い領域を左ドラッグします。
   - 位置をリセットする場合には Document/HotsTalentDescription/setting.xml を削除してください。


## 自動表示の設定

 - ボタンを右クリックして「設定」フォームを表示します。
 - 「調整」を行い「確認」に１番下のオリジナルタレントの名前が表示されるようにします。

 - 自動表示はヒーローを変更して「？」ボタンを押すと解除されます。
   - 再び自動表示を行うためにはヒーロー名のコンボボックスをクリックしてください。


## インストール手順

 - Python 2.7 系の最新版を全て「次へ」でインストールします。
   - https://www.python.org/ftp/python/2.7.15/python-2.7.15.amd64.msi
 - script/update.vbs を実行する。（数分かかります）


## ライセンス

 - MIT ライセンス


## 連絡先

 - バグや要望がありましたら、開発サイトの課題トラッカーにお知らせ下さい。
   - https://bitbucket.org/satofumi/hots_tools/issues
