# タレントの内容を日本語で表示してくれるツール

## 動作

 - タレントが選択できるときに、タレントの内容を日本語メッセージで表示できる。
   - タレントが選択できるようになったときに、画面左上に「？」ボタンを配置する。
   - 「？」ボタンを押すと、タレント個々の説明メッセージが表示される。
     - 各タレントに対応するアイコンを表示してもよい。
   - タレントを選択すると自動的にメッセージ表示は隠され「？」ボタンのみの表示に戻る。
     

## 実装

  - 自分が選択したヒーローは %AppData%/Local/Temp/Heroes of the Storm/TempWriteReplayP1/replay.server.battlelobby の情報から取得する。
    - あらかじめ自分の Blizzard のプレイヤー名を登録する必要がある。
    - ファイルのパースには https://github.com/barrett777/Heroes.ReplayParser/blob/master/Heroes.ReplayParser/MPQFiles/ReplayServerBattlelobby.cs を利用する。

  - タレントが選択できるかどうかは、左下のキャプチャ画像で判別する。
    - ヒーローのレベルは画面中央の数値から取得し、対応するタレントを表示できるようにする。

  - タレントの情報は HotS 日本語 Wiki から取得して利用できるようにする。
    - 情報の更新は Python スクリプトで行い、必要なデータを YAML フォーマットで保存して利用する。

## データファイル

  - YAML フォーマットで定義する。
  - 画像保存用のディレクトリを用意して、画像はそこにまとめる。
  - ファイル名をヒーロー名にする。

talent:
  - 1:
    - icon_path, name, type_path, description
    - ...
    - ...
  - 2:
    ...
