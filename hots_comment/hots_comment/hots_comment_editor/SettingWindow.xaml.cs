﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace hots_comment_editor
{
    /// <summary>
    /// SettingWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class SettingWindow : Window
    {
        public event EventHandler CommentFontSizeChanged;


        public SettingWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Collapsed;
        }

        public int CommentTextFontSize
        {
            set
            {
                FontSizeUpDown.Value = value;
                SampleTextLabel.FontSize = value;
            }
            get
            {
                return FontSizeUpDown.Value;
            }
        }

        private void FontSizeUpDown_ValueChanged(object sender, EventArgs e)
        {
            int value = FontSizeUpDown.Value;
            SampleTextLabel.FontSize = value;
            CommentFontSizeChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
