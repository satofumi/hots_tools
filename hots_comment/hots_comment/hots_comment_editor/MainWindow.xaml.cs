﻿using HotsTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace hots_comment_editor
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public static RoutedCommand SaveCommand = new RoutedCommand();

        private readonly string HotsCommentDataFolder = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "HotsTools", "Comment");
        private readonly TimeSpan InvalidTimeSpan = new TimeSpan(0, 0, -1);

        private enum CaptureState : int
        {
            Invalid = 0,
            Valid = 1,
            NewReplay = 2,
        }

        private class ProjectFile
        {
            public string FilePath { get; set; }
            public string FileName
            {
                get
                {
                    return System.IO.Path.GetFileNameWithoutExtension(FilePath);
                }
            }

            public ProjectFile(string filePath)
            {
                FilePath = filePath;
            }

            public override bool Equals(object obj)
            {
                var other = obj as ProjectFile;
                if (other == null)
                {
                    return false;
                }
                return (FilePath == other.FilePath) ? true : false;
            }

            public override int GetHashCode()
            {
                return FilePath.GetHashCode();
            }
        }

        private CommentsListWindow commentsListWindow = new CommentsListWindow();
        private CommentsTextWindow commentsTextWindow = new CommentsTextWindow();
        private ClipAdjustWindow clipAdjustWindow = new ClipAdjustWindow();
        private SettingWindow settingWindow = new SettingWindow();
        private AboutApplicationWindow aboutApplicationWindow = new AboutApplicationWindow();
        private BackgroundWorker worker = new BackgroundWorker();
        private CaptureReplayCounts captureReplayCounts = new CaptureReplayCounts();
        private ClipArea clipArea = new ClipArea();
        private TimeSpan totalTimeSpan = new TimeSpan(0, 0, -1);
        private TimeSpan currentTimeSpan = new TimeSpan(0, 0, -1);
        private TimeSpan previousTimeSpan = new TimeSpan(0, 0, -1);
        private CommentManager comments = new CommentManager();
        private string saveProjectFilePath = "";
        private bool commentEdited;
        private string titleText;
        private string settingFilePath;
        private SettingData settingData;


        public MainWindow()
        {
            InitializeComponent();

#if VIEWER
            SetupViewer();
#endif
            InitializeUi();
            LoadConfig();
            LoadClipSetting();
            StartCapture();
        }

        private void InitializeUi()
        {
            titleText = Title;
            SaveCommand.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            commentsListWindow.CommentRemoved += CommentsListWindow_CommentRemoved;
            commentsListWindow.Closing += CommentsListWindow_Closing;
            clipAdjustWindow.ClipStart += ClipAdjustWindow_ClipStart;
            clipAdjustWindow.Closing += ClipAdjustWindow_Closing;
            captureReplayCounts.ImageChanged += clipAdjustWindow.CaptureImage;
            settingWindow.CommentFontSizeChanged += SettingWindow_CommentFontSizeChanged;
            settingWindow.Closing += SettingWindow_Closing;
        }

        private void SettingWindow_CommentFontSizeChanged(object sender, EventArgs e)
        {
            commentsTextWindow.CommentFontSize = settingWindow.CommentTextFontSize;
        }

        private void SettingWindow_Closing(object sender, CancelEventArgs e)
        {
            commentsTextWindow.Resizable = false;
        }

        private void ClipAdjustWindow_Closing(object sender, CancelEventArgs e)
        {
            captureReplayCounts.NortifyImage = false;
        }

        private void CommentsListWindow_Closing(object sender, EventArgs e)
        {
            CommentsListButton.IsChecked = false;
        }

        private void CommentsListWindow_CommentRemoved(object sender, CommentsListWindow.CommentRemovedEventArgs e)
        {
            comments.Remove(e.Removed);
            commentsTextWindow.Comments = comments;
            SetEdited(true);
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (commentEdited)
            {
                if (!DiscardChanges())
                {
                    return;
                }
            }

            StopCapture();
            SaveClipSetting();
            SaveConfig();

            commentsTextWindow.Close();
            commentsListWindow.Close();
            clipAdjustWindow.Close();
            settingWindow.Close();
            aboutApplicationWindow.Close();

            System.Windows.Application.Current.Shutdown();
        }

        private void LoadConfig()
        {
            var settings = Properties.Settings.Default;

            this.Width = settings.WindowRectW;
            this.Height = settings.WindowRectH;
            this.Left = settings.WindowRectX;
            this.Top = settings.WindowRectY;

            commentsListWindow.Width = settings.CommentsListWindowRectW;
            commentsListWindow.Height = settings.CommentsListWindowRectH;
            commentsListWindow.Left = settings.CommentsListWindowRectX;
            commentsListWindow.Top = settings.CommentsListWindowRectY;

            commentsTextWindow.Width = settings.CommentsTextWindowW;
            commentsTextWindow.Height = settings.CommentsTextWindowH;
            commentsTextWindow.Left = settings.CommentsTextWindowLeft;
            commentsTextWindow.Top = settings.CommentsTextWindowBottom - commentsTextWindow.Height;
            commentsTextWindow.FontSize = settings.CommentsTextFontSize;

            settingWindow.CommentTextFontSize = settings.CommentsTextFontSize;

            CommentsButton.IsChecked = settings.CommentsTextWindowsVisible;
            if (CommentsButton.IsChecked.HasValue && (bool)CommentsButton.IsChecked)
            {
                commentsTextWindow.Show();
            }

            CommentsListButton.IsChecked = settings.CommentsListWindowVisible;
            if (CommentsListButton.IsChecked.HasValue && (bool)CommentsListButton.IsChecked)
            {
                commentsListWindow.Show();
            }

            if (string.IsNullOrEmpty(settings.ProjectFolder))
            {
                settings.ProjectFolder = HotsCommentDataFolder;
            }
        }

        private void SaveConfig()
        {
            if (WindowState == WindowState.Minimized)
            {
                return;
            }

            var settings = Properties.Settings.Default;

            settings.WindowRectW = this.Width;
            settings.WindowRectH = this.Height;
            settings.WindowRectX = this.Left;
            settings.WindowRectY = this.Top;

            settings.CommentsListWindowRectW = commentsListWindow.Width;
            settings.CommentsListWindowRectH = commentsListWindow.Height;
            settings.CommentsListWindowRectX = commentsListWindow.Left;
            settings.CommentsListWindowRectY = commentsListWindow.Top;

            settings.CommentsTextWindowW = commentsTextWindow.Width;
            settings.CommentsTextWindowH = commentsTextWindow.Height;
            settings.CommentsTextWindowLeft = commentsTextWindow.Left;
            settings.CommentsTextWindowBottom = commentsTextWindow.Top + commentsTextWindow.Height;
            settings.CommentsTextFontSize = commentsTextWindow.CommentFontSize;

            settings.CommentsTextWindowsVisible = CommentsButton.IsChecked.HasValue ? (bool)CommentsButton.IsChecked : false;
            settings.CommentsListWindowVisible = CommentsListButton.IsChecked.HasValue ? (bool)CommentsListButton.IsChecked : false;

            settings.Save();
        }

        private void LoadClipSetting()
        {
            var settings = Properties.Settings.Default;

            settingFilePath = System.IO.Path.Combine(HotsCommentDataFolder, "setting.xml");
            if (!File.Exists(settingFilePath))
            {
                settingData = new SettingData();
                settingData.ClipArea = new System.Drawing.Rectangle(settings.ClipRectX, settings.ClipRectY, settings.ClipRectW, settings.ClipRectH);
                SaveClipSetting();
            }
            else
            {
                settingData = XmlAccessor.Load(typeof(SettingData), settingFilePath) as SettingData;
            }

            clipAdjustWindow.ClipRectangle = settingData.ClipArea;
            clipArea.Clipped += ClipArea_Clipped;
            clipAdjustWindow.ClipAreaChanged += ClipArea_Clipped;
        }

        private void SaveClipSetting()
        {
            Directory.CreateDirectory(HotsCommentDataFolder);
            XmlAccessor.Save(typeof(SettingData), settingFilePath, settingData);
        }

        private void ClipArea_Clipped(object sender, ClipArea.ClippedEventArgs e)
        {
            settingData.ClipArea = e.rectangle;
            clipAdjustWindow.ClipRectangle = settingData.ClipArea;
        }

        private void StartCapture()
        {
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerAsync();
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var state = (CaptureState)e.ProgressPercentage;
            if (state == CaptureState.NewReplay)
            {
#if VIEWER
                AutoLoadProjectFile();
#endif
            }
            else
            {
                UpdateCounter(state);
            }
        }

        private void AutoLoadProjectFile()
        {
            int totalSeconds = (int)totalTimeSpan.TotalSeconds;
            var files = FindProjectFileByTotalSeconds(totalSeconds);
            if (files.Count == 0)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(saveProjectFilePath))
            {
                // データが読み込まれてないときに読み込みを行う。
                ProjectFilesComboBox.ItemsSource = files;

                if (files.Count == 1)
                {
                    // マッチしたファイルを読み込ませる。
                    ProjectFilesComboBox.SelectedIndex = 0;
                }
            }
            else
            {
                // コンボボックスへマッチしたファイルの追加を行う。
                var currentProjectFile = ProjectFilesComboBox.SelectedItem as ProjectFile;
                InsertItemToProjectsComboBox(currentProjectFile);
                ProjectFilesComboBox.SelectedIndex = 0;
            }
        }

        private void InsertItemToProjectsComboBox(ProjectFile projectFile)
        {
            var files = ProjectFilesComboBox.ItemsSource as List<ProjectFile>;
            if (files == null)
            {
                files = new List<ProjectFile>();
            }
            if (!files.Contains(projectFile))
            {
                files.Insert(0, projectFile);
            }
            ProjectFilesComboBox.ItemsSource = files;
        }

        private List<ProjectFile> FindProjectFileByTotalSeconds(int totalSeconds)
        {
            string projectFolder = Properties.Settings.Default.ProjectFolder;
            if (string.IsNullOrWhiteSpace(projectFolder))
            {
                return new List<ProjectFile>();
            }

            if (!System.IO.Directory.Exists(projectFolder))
            {
                return new List<ProjectFile>();
            }

            // 更新日時が新しい順にソートして返す
            var directory = new DirectoryInfo(projectFolder);
            var files = directory.GetFiles("*.yaml");
            Array.Sort(files, delegate (FileInfo f1, FileInfo f2)
            {
                return f2.LastWriteTime.CompareTo(f1.LastWriteTime);
            });

            var projectFiles = new List<ProjectFile>();
            foreach (var fileInfo in files)
            {
                string filePath = fileInfo.FullName;
                int fileTotalSeconds = ProjectFileAccessor.ProjectFileTotalSeconds(filePath);
                if (fileTotalSeconds == totalSeconds)
                {
                    projectFiles.Add(new ProjectFile(filePath));
                }
            }
            return projectFiles;
        }

        private void ProjectFilesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var project = ProjectFilesComboBox.SelectedItem as ProjectFile;
            if (project == null)
            {
                return;
            }

            LoadProjectFile(project.FilePath);
        }

        private void UpdateCounter(CaptureState state)
        {
            if (state == CaptureState.Valid)
            {
                UpdateCountText(currentTimeSpan);
            }
            else
            {
                CounterLabel.Content = "XX:XX";
            }

            bool valid = (state == CaptureState.Valid) ? true : false;
            CounterLabel.IsEnabled = valid;
            CommentTextBox.IsEnabled = valid;

            if (previousTimeSpan.TotalSeconds != currentTimeSpan.TotalSeconds)
            {
                int totalSeconds = (int)currentTimeSpan.TotalSeconds;
                commentsTextWindow.CurrentTotalSeconds = totalSeconds;
                commentsListWindow.CurrentTotalSeconds = totalSeconds;
                previousTimeSpan = currentTimeSpan;
            }
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!worker.CancellationPending)
            {
                CaptureCounts(settingData.ClipArea);
                System.Threading.Thread.Sleep(25);
            }
            e.Cancel = true;
        }

        private void StopCapture()
        {
            if (worker.WorkerSupportsCancellation)
            {
                worker.CancelAsync();
            }
        }

        private void CaptureCounts(System.Drawing.Rectangle rectangle)
        {
            var current = InvalidTimeSpan;
            var total = InvalidTimeSpan;
            captureReplayCounts.Capture(ref current, ref total, rectangle);

            if (clipAdjustWindow.Visibility == Visibility.Visible)
            {
                clipAdjustWindow.CurrentText = current;
                clipAdjustWindow.TotalText = total;
            }

            bool valid = (current.TotalSeconds >= 0) ? true : false;
            if (valid)
            {
                currentTimeSpan = current;
            }
            worker.ReportProgress(valid ? (int)CaptureState.Valid : (int)CaptureState.Invalid);

            if (total.TotalSeconds >= 0)
            {
#if VIEWER
                if ((totalTimeSpan.TotalSeconds != total.TotalSeconds) && (total.TotalSeconds >= 0))
                {
                    // 違うリプレイデータを再生し始めた可能性があるので、情報を更新する
                    worker.ReportProgress((int)CaptureState.NewReplay);
                }
#endif

                totalTimeSpan = total;
            }
        }

        private void UpdateCountText(TimeSpan current)
        {
            CounterLabel.Content = TimeText.ToText(current);
        }

        private void SetupViewer()
        {
            Title = "HotS Comment Viewer";
            var iconUri = new Uri("pack://application:,,,/Resources/comment.png");
            Icon = BitmapFrame.Create(iconUri);
            commentsListWindow.Icon = Icon;

            commentsListWindow.IsEditable = false;

            NewProjectButton.Visibility = Visibility.Collapsed;
            SaveProjectButton.Visibility = Visibility.Collapsed;
            CommentsButton.Visibility = Visibility.Collapsed;
            CommentTextBox.Visibility = Visibility.Collapsed;

            commentsTextWindow.Show();
            ProjectFilesComboBox.Visibility = Visibility.Visible;
        }

        private void SaveProjectFile(string filePath)
        {
            Properties.Settings.Default.ProjectFolder = System.IO.Path.GetDirectoryName(filePath);
            saveProjectFilePath = filePath;
            SetEdited(false);
            comments.TotalSeconds = (int)totalTimeSpan.TotalSeconds;
            ProjectFileAccessor.Save(filePath, comments);
        }

        private void LoadProjectFile(string filePath)
        {
            Properties.Settings.Default.ProjectFolder = System.IO.Path.GetDirectoryName(filePath);
            saveProjectFilePath = filePath;
            SetEdited(false);
            comments = ProjectFileAccessor.Load(filePath);
            UpdateComments();
        }

        private void AddComment(int seconds, string commentText)
        {
            if (string.IsNullOrEmpty(commentText))
            {
                return;
            }

            comments.Add(new Comment(seconds, commentText));
            UpdateComments();

            if (!commentEdited)
            {
                SetEdited(true);
            }
        }

        private void SetEdited(bool edited)
        {
            Title = titleText + (edited ? "*" : "");
            commentEdited = edited;
        }

        private bool DiscardChanges()
        {
            var result = System.Windows.Forms.MessageBox.Show("Discard changes?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            return (result == System.Windows.Forms.DialogResult.OK) ? true : false;
        }

        private void NewProjectIcon_Click(object sender, RoutedEventArgs e)
        {
            if (commentEdited)
            {
                if (!DiscardChanges())
                {
                    return;
                }
            }

            saveProjectFilePath = "";
            SetEdited(false);
            comments.Clear();
            UpdateComments();
        }

        private void UpdateComments()
        {
            commentsListWindow.Comments = comments;
            commentsTextWindow.Comments = comments;

            // 追加したコメント位置にスクロールさせるために時間を再登録する
            commentsListWindow.CurrentTotalSeconds = (int)currentTimeSpan.TotalSeconds;
        }

        private void SaveCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(saveProjectFilePath))
            {
                SaveProject();
            }
            else
            {
                SaveProjectFile(saveProjectFilePath);
            }
        }

        private void OpenProjectIcon_Click(object sender, RoutedEventArgs e)
        {
            if (commentEdited)
            {
                if (!DiscardChanges())
                {
                    return;
                }
            }

            var dialog = new OpenFileDialog
            {
                Filter = "YAML Files|*.yaml",
                Title = "Select project file",
                InitialDirectory = Properties.Settings.Default.ProjectFolder
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
#if VIEWER
                var projectFile = new ProjectFile(dialog.FileName);
                InsertItemToProjectsComboBox(projectFile);
                ProjectFilesComboBox.SelectedItem = new ProjectFile(dialog.FileName);
#else
                LoadProjectFile(dialog.FileName);
#endif
            }
        }

        private void SaveProjectIcon_Click(object sender, RoutedEventArgs e)
        {
            SaveProject();
        }

        private void SaveProject()
        {
            var dialog = new SaveFileDialog
            {
                Title = "Save project file",
                Filter = "YAML File|*.yaml",
                InitialDirectory = Properties.Settings.Default.ProjectFolder
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SaveProjectFile(dialog.FileName);
            }
        }

        private void CommentsIcon_Click(object sender, RoutedEventArgs e)
        {
            commentsTextWindow.Visibility = (commentsTextWindow.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void CommentListIcon_Click(object sender, RoutedEventArgs e)
        {
            commentsListWindow.Visibility = (commentsListWindow.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void ClipAreaIcon_Click(object sender, RoutedEventArgs e)
        {
            clipAdjustWindow.Show();
            captureReplayCounts.NortifyImage = true;
        }

        private void ClipAdjustWindow_ClipStart(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                clipArea.StartClip();
            });
        }

        private void SettingIcon_Click(object sender, RoutedEventArgs e)
        {
            settingWindow.Show();
            commentsTextWindow.Resizable = true;
        }

        private void InformationIcon_Click(object sender, RoutedEventArgs e)
        {
            aboutApplicationWindow.Show();
        }

        private void CommentTextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                int seconds = (int)currentTimeSpan.TotalSeconds;
                string comment = CommentTextBox.Text;
                AddComment(seconds, comment);
                CommentTextBox.Clear();
            }
        }

        private void CommentTextBox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                CommentTextBox.CaretIndex = 0;
            }
            else if (e.Key == Key.Down)
            {
                CommentTextBox.CaretIndex = CommentTextBox.Text.Length;
            }
        }
    }
}
