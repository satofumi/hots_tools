﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace hots_comment_editor
{
    /// <summary>
    /// CommentsListWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class CommentsListWindow : Window
    {
        public class CommentRemovedEventArgs : EventArgs
        {
            public Comment Removed;

            public CommentRemovedEventArgs(Comment comment)
            {
                Removed = comment;
            }
        }

        public event EventHandler<CommentRemovedEventArgs> CommentRemoved;


        public CommentsListWindow()
        {
            InitializeComponent();
            IsEditable = true;
        }

        public bool IsEditable { get; set; }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Collapsed;
        }

        public int CurrentTotalSeconds
        {
            set
            {
                foreach (Comment data in CommentListBox.Items)
                {
                    if (data.Seconds == value)
                    {
                        CommentListBox.SelectedItem = data;
                        CommentListBox.ScrollIntoView(data);
                    }
                }
            }
        }

        public CommentManager Comments
        {
            set
            {
                var items = new List<Comment>();
                foreach (var data in value)
                {
                    items.Add(new Comment(data.Key, data.Value));
                }
                CommentListBox.ItemsSource = items;
            }
        }

        private void MenuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            if (!IsEditable)
            {
                return;
            }

            foreach (Comment item in CommentListBox.SelectedItems)
            {
                (CommentListBox.ItemsSource as List<Comment>).Remove(item);
                CommentRemoved?.Invoke(this, new CommentRemovedEventArgs(item));
            }
            CommentListBox.Items.Refresh();
        }
    }
}
