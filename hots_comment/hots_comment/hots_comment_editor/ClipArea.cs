﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hots_comment_editor
{
    public class ClipArea
    {
        public class ClippedEventArgs : EventArgs
        {
            public Rectangle rectangle;

            public ClippedEventArgs(Rectangle rectangle)
            {
                this.rectangle = rectangle;
            }
        }

        public event EventHandler<ClippedEventArgs> Clipped;

        public ClipArea()
        {
        }

        public void StartClip()
        {
            const int waitMsec = 25;

            // 直近のクリックがリリースされるまで待機する
            WaitReleased(waitMsec);

            // クリックされるまで待機する
            WaitClicked(waitMsec);
            var startPoint = Cursor.Position;

            // リリースされるまで待機する
            WaitReleased(waitMsec);
            var stopPoint = Cursor.Position;

            // ドラッグされた領域を通知する
            int left = startPoint.X;
            int right = stopPoint.X;
            if (left > right)
            {
                Swap<int>(ref left, ref right);
            }

            int top = startPoint.Y;
            int bottom = stopPoint.Y;
            if (top > bottom)
            {
                Swap<int>(ref top, ref bottom);
            }

            Clipped?.Invoke(this, new ClippedEventArgs(new Rectangle(left, top, right - left, bottom - top)));
        }

        private void WaitClicked(int waitMsec)
        {
            while ((Control.MouseButtons & MouseButtons.Left) != MouseButtons.Left)
            {
                System.Threading.Thread.Sleep(waitMsec);
            }
        }

        private void WaitReleased(int waitMsec)
        {
            while ((Control.MouseButtons & MouseButtons.Left) == MouseButtons.Left)
            {
                System.Threading.Thread.Sleep(waitMsec);
            }
        }

        public static void Swap<T>(ref T a, ref T b)
        {
            T temp = a;
            a = b;
            b = temp;
        }
    }
}
