﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace hots_comment_editor
{
    /// <summary>
    /// CommentsWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class CommentsTextWindow : Window
    {
        private const int OverMaxLines = 20;
        private CommentManager comments;
        private int currentTotalSeconds;


        public CommentsTextWindow()
        {
            InitializeComponent();
            TextHoldSeconds = 8;

            MoveLabel.MouseLeftButtonDown += (sender, e) => { this.DragMove(); };
        }

        public CommentManager Comments
        {
            set
            {
                comments = value;
                Update();
            }
        }

        public int CurrentTotalSeconds
        {
            set
            {
                currentTotalSeconds = value;
                Update();
            }
        }

        public int TextHoldSeconds { private get; set; }

        private void Update()
        {
            if (comments == null)
            {
                return;
            }

            // 改行で埋める
            string text = "";
            for (int i = 0; i < OverMaxLines; ++i)
            {
                text += Environment.NewLine;
            }
            CommentsTextBlock.Text = text;

            foreach (var data in comments)
            {
                int commentSeconds = data.Key;
                if ((currentTotalSeconds < commentSeconds) || (currentTotalSeconds >= (commentSeconds + TextHoldSeconds)))
                {
                    continue;
                }

                CommentsTextBlock.Text += Environment.NewLine + data.Value;
            }
            CommentsScrollViewer.ScrollToBottom();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Collapsed;
        }

        public bool Resizable
        {
            set
            {
                Background = new SolidColorBrush(value ? Colors.DarkGray : Colors.Transparent);
                ResizeMode = value ? ResizeMode.CanResizeWithGrip : ResizeMode.NoResize;
            }
        }

        public int CommentFontSize
        {
            set
            {
                CommentsTextBlock.FontSize = value;
            }
            get
            {
                return (int)CommentsTextBlock.FontSize;
            }
        }
    }
}
