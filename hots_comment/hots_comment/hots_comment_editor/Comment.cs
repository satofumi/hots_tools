﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hots_comment_editor
{
    public class Comment
    {
        public int Seconds { get; set; }
        public string SecondsTime
        {
            get
            {
                var timeSpan = new TimeSpan(0, 0, Seconds);
                return TimeText.ToText(timeSpan);
            }
        }
        public string CommentText { get; set; }

        public Comment(int seconds, string commentText)
        {
            Seconds = seconds;
            CommentText = commentText;
        }
    }
}
