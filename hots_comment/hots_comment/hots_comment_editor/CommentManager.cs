﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hots_comment_editor
{
    public class CommentManager : IEnumerable<KeyValuePair<int, string>>
    {
        private SortedDictionary<int, List<string>> comments = new SortedDictionary<int, List<string>>();

        public int TotalSeconds { get; set; }

        public void Clear()
        {
            comments.Clear();
        }

        public void Add(Comment comment)
        {
            int seconds = comment.Seconds;
            string text = comment.CommentText;

            if (!comments.ContainsKey(seconds))
            {
                comments[seconds] = new List<string>();
            }

            comments[seconds].Add(text);
        }

        public void Remove(Comment comment)
        {
            int seconds = comment.Seconds;
            string text = comment.CommentText;

            if (!comments.ContainsKey(seconds))
            {
                return;
            }

            comments[seconds].Remove(text);
        }

        public IEnumerator<KeyValuePair<int, string>> GetEnumerator()
        {
            foreach (var secondsComments in comments)
            {
                int seconds = secondsComments.Key;
                foreach (var comment in secondsComments.Value)
                {
                    yield return new KeyValuePair<int, string>(seconds, comment);
                }

            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
