﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace hots_comment_editor
{
    /// <summary>
    /// UpDownControl.xaml の相互作用ロジック
    /// </summary>
    public partial class UpDownControl : UserControl
    {
        public event EventHandler ValueChanged;

        private int intValue;

        public UpDownControl()
        {
            InitializeComponent();
        }

        public void SetValue(int value)
        {
            intValue = value;
            NumberTextBox.Dispatcher.BeginInvoke(
                new Action(() =>
                {
                    NumberTextBox.Text = intValue.ToString();
                }));
        }

        public int Value
        {
            get
            {
                return intValue;
            }
            set
            {
                bool changed = (intValue == value) ? false : true;
                SetValue(value);

                if (changed)
                {
                    ValueChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        private void MinusButton_Click(object sender, RoutedEventArgs e)
        {
            Value -= 1;
        }

        private void PlusButton_Click(object sender, RoutedEventArgs e)
        {
            Value += 1;
        }
    }
}
