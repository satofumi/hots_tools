﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace hots_comment_editor
{
    /// <summary>
    /// ClipAdjustWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ClipAdjustWindow : Window
    {
        [DllImport("gdi32")]
        static extern int DeleteObject(IntPtr o);

        public event EventHandler ClipStart;
        public event EventHandler<ClipArea.ClippedEventArgs> ClipAreaChanged;

        private System.Drawing.Rectangle clipRectangle;


        public ClipAdjustWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ClipStart?.Invoke(this, EventArgs.Empty);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Collapsed;
        }

        public System.Drawing.Rectangle ClipRectangle
        {
            get
            {
                return clipRectangle;
            }
            set
            {
                UpDownX.SetValue(value.Left);
                UpDownY.SetValue(value.Top);
                UpDownW.SetValue(value.Width);
                UpDownH.SetValue(value.Height);

                clipRectangle = value;
            }
        }

        private void UpdateRectangle()
        {
            var rectangle = new System.Drawing.Rectangle(UpDownX.Value, UpDownY.Value, UpDownW.Value, UpDownH.Value);
            ClipAreaChanged?.Invoke(this, new ClipArea.ClippedEventArgs(rectangle));
        }

        private void UpDownX_ValueChanged(object sender, EventArgs e)
        {
            UpdateRectangle();
        }

        private void UpDownY_ValueChanged(object sender, EventArgs e)
        {
            UpdateRectangle();
        }

        private void UpDownW_ValueChanged(object sender, EventArgs e)
        {
            UpdateRectangle();
        }

        private void UpDownH_ValueChanged(object sender, EventArgs e)
        {
            UpdateRectangle();
        }

        public TimeSpan CurrentText
        {
            set
            {
                UpdateTimeText(CurrentTextLabel, CurrentResultLabel, value);
            }
        }

        public TimeSpan TotalText
        {
            set
            {
                UpdateTimeText(TotalTextLabel, TotalResultLabel, value);
            }
        }

        private void UpdateTimeText(Label textLabel, Label resultLabel, TimeSpan value)
        {
            string text = (value.TotalSeconds < 0) ? "XX:XX" : TimeText.ToText(value);

            textLabel.Dispatcher.BeginInvoke(
                        new Action(() =>
                        {
                            textLabel.Content = text;
                        }));

            string resultText = (value.TotalSeconds < 0) ? "NG" : "OK";
            resultLabel.Dispatcher.BeginInvoke(
                        new Action(() =>
                        {
                            resultLabel.Content = resultText;
                        }));
        }

        internal void CaptureImage(object sender, CaptureReplayCounts.ImageChangedEventArgs e)
        {
            CheckImage.Dispatcher.BeginInvoke(
                new Action(() =>
                {
                    CheckImage.Source = LoadBitmap(e.Image);
                }));
        }

        public static BitmapSource LoadBitmap(System.Drawing.Bitmap source)
        {
            IntPtr ip = source.GetHbitmap();
            BitmapSource bs = null;
            try
            {
                bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ip, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }
            finally
            {
                DeleteObject(ip);
            }

            return bs;
        }
    }
}
