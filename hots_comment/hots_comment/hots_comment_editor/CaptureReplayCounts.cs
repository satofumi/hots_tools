﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace hots_comment_editor
{
    class CaptureReplayCounts
    {
        public class ImageChangedEventArgs : EventArgs
        {
            public Bitmap Image;

            public ImageChangedEventArgs(Bitmap image)
            {
                Image = image;
            }
        }

        public event EventHandler<ImageChangedEventArgs> ImageChanged;


        public bool NortifyImage { get; set; }

        public void Capture(ref TimeSpan currentCount, ref TimeSpan totalCount, Rectangle rectangle)
        {
            string countText = ParseFromImage(rectangle).Trim().Replace(" ", "");
            var tokens = countText.Split('/');
            if (tokens.Count() != 2)
            {
                return;
            }
            currentCount = ParseCountText(tokens[0]);
            totalCount = ParseCountText(tokens[1]);
        }

        private TimeSpan ParseCountText(string countText)
        {
            var values = countText.Split(':');
            if (values.Count() != 2)
            {
                return new TimeSpan(0, 0, -1);
            }

            try
            {
                int minutes = int.Parse(values[0]);
                int seconds = int.Parse(values[1]);
                return new TimeSpan(0, minutes, seconds);
            }
            catch (Exception)
            {
                return new TimeSpan(0, 0, -1);
            }
        }

        private string ParseFromImage(Rectangle rectangle)
        {
            var image = CaptureImage(rectangle);
            if (NortifyImage)
            {
                ImageChanged?.Invoke(this, new ImageChangedEventArgs(image));
            }
            using (var tesseract = new Tesseract.TesseractEngine("tessdata", "eng"))
            {
                tesseract.SetVariable("tessedit_char_whitelist", "0123456789:/");
                Tesseract.Page page = tesseract.Process(image);
                return page.GetText();
            }
        }

        private Bitmap CaptureImage(Rectangle rectangle)
        {
            var screenBmp = new Bitmap(rectangle.Width, rectangle.Height, PixelFormat.Format32bppArgb);
            using (var bmpGraphics = Graphics.FromImage(screenBmp))
            {
                bmpGraphics.CopyFromScreen(rectangle.X, rectangle.Y, 0, 0, screenBmp.Size);
                return screenBmp;
            }
        }
    }
}
