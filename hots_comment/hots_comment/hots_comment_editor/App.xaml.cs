﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace hots_comment_editor
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        [STAThread]
        public static void Main()
        {
            const string semaphoreName = "HotsComment";
            bool created;
            using (var semaphore = new System.Threading.Semaphore(1, 1, semaphoreName, out created))
            {
                if (!created)
                {
                    MessageBox.Show("Application already exists.", "Critical", MessageBoxButton.OK, MessageBoxImage.Stop);
                    Current.Shutdown();
                }

                App app = new App();
                app.InitializeComponent();
                app.Run();
            }
        }
    }
}
