﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace hots_comment_editor
{
    public static class ProjectFileAccessor
    {
        private const string FormatVersion = "1.0.0";

        public static void Save(string filePath, CommentManager comments)
        {
            using (StreamWriter file = new StreamWriter(filePath))
            {
                file.WriteLine("version: " + FormatVersion);
                file.WriteLine("total_seconds: " + comments.TotalSeconds.ToString());

                file.WriteLine("comments:");
                foreach (var data in comments)
                {
                    file.WriteLine("  - " + data.Key.ToString() + ", '" + data.Value + "'");
                }
            }
        }

        public static CommentManager Load(string filePath)
        {
            var comments = new CommentManager();

            var input = new StreamReader(filePath, Encoding.UTF8);
            var yaml = new YamlStream();
            yaml.Load(input);

            var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;
            string format_version = ((YamlScalarNode)mapping.Children[new YamlScalarNode("version")]).Value;
            if (format_version != FormatVersion)
            {
                throw new Exception("missmatch data format.");
            }

            string secondsText = ((YamlScalarNode)mapping.Children[new YamlScalarNode("total_seconds")]).Value;
            comments.TotalSeconds = int.Parse(secondsText);

            var commentNodes = (YamlSequenceNode)mapping.Children[new YamlScalarNode("comments")];
            foreach(YamlScalarNode line in commentNodes)
            {
                var tokens = line.Value.Split(new char[] { ',' }, 2);
                int seconds = int.Parse(tokens[0]);
                string comment = tokens[1].Trim(new char[] { ' ', '\'' });
                comments.Add(new Comment(seconds, comment));
            }

            return comments;
        }

        public static int ProjectFileTotalSeconds(string filePath)
        {
            var data = Load(filePath);
            return data.TotalSeconds;
        }
    }
}
