# HotS Comment

Heroes of the Storm のリプレイファイルにコメントを付加するためのツールです。
リプレイを表示しながらツールを使用します。

  * HotS Comment Editor ... コメントを編集する。
  * HotS Comment Reader ... 編集されたコメントを再生する。


## License

  * MIT


## Web page

  * http://hyakuren-soft.sakura.ne.jp/hots_tutorial/hots_comment.html


## Development page
  * https://bitbucket.org/satofumi/hots_tools


### ビルド方法

Visual Studio 2017 でビルドし、tesseract-ocr-3.02.eng.tar 内にある
tessdata/eng.traineddata 実行ファイルのフォルダにコピーして利用してください。

例)
hots_editor/bin/Release/tessdata/eng.traineddata


# Resources
  * FatCow (http://www.fatcow.com/free-icons)
      * Creative Commons Attribution 3.0 License
