﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tobii.StreamEngine;

namespace PlotPositionSample
{
    class Program
    {
        static void Main(string[] args)
        {
            // 初期化
            var error = Interop.tobii_api_create(out IntPtr api, null);
            ErrorExit(error);

            // バージョン情報の取得
            error = Interop.tobii_get_api_version(out tobii_version_t version);
            ErrorExit(error);
            Console.WriteLine(string.Format("{0}.{1}.{2} ({3})", version.major, version.minor, version.revision, version.build));

            // デバイスリストの取得
            error = Interop.tobii_enumerate_local_device_urls(api, out List<string> deviceNames);
            ErrorExit(error);
            foreach (var name in deviceNames)
            {
                Console.WriteLine(name);
            }

            // デバイスハンドラの作成
            error = Interop.tobii_device_create(api, deviceNames[0], out IntPtr device);
            ErrorExit(error);

            // エンジンの作成
            error = Interop.tobii_engine_create(api, out IntPtr engine);
            ErrorExit(error);

            // 視線の取得
            error = Interop.tobii_gaze_point_subscribe(device, GazePointCallback);
            var devices = new IntPtr[] { device };
            while (true)
            {
                error = Interop.tobii_wait_for_callbacks(engine, devices);
                ErrorExit(error);
                if ((error != tobii_error_t.TOBII_ERROR_NO_ERROR) && (error != tobii_error_t.TOBII_ERROR_TIMED_OUT))
                {
                    ErrorExit(error);
                }

                error = Interop.tobii_device_process_callbacks(device);
                ErrorExit(error);
            }

            System.Threading.Thread.Sleep(500);


            // エンジンの破棄
            error = Interop.tobii_engine_destroy(engine);
            ErrorExit(error);

            // デバイスハンドラの破棄
            error = Interop.tobii_device_destroy(device);
            ErrorExit(error);

            // 終了処理
            error = Interop.tobii_api_destroy(api);
            ErrorExit(error);
            while (true)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        private static void GazePointCallback(ref tobii_gaze_point_t gazePoint)
        {
            Console.WriteLine(string.Format("{0}: {1}, {2}", gazePoint.timestamp_us, gazePoint.position.x, gazePoint.position.y));
        }

        private static void ErrorExit(tobii_error_t error)
        {
            if (error != tobii_error_t.TOBII_ERROR_NO_ERROR)
            {
                Console.WriteLine("error: " + Interop.tobii_error_message(error));

                while (true)
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }
    }
}
