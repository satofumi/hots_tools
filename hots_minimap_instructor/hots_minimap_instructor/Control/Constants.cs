﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsMinimapInstructor.Control
{
    public static class Constants
    {
        public static readonly string SettingsFilePath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "HotsTools/MinimapInstructor/setting.xml");
    }
}
