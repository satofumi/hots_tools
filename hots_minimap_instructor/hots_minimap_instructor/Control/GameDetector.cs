﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsMinimapInstructor
{
    public static class GameDetector
    {
        private static readonly string hotsTempFolderPath = Path.Combine(Path.GetTempPath(), "Heroes of the Storm\\TempWriteReplayP1");


        public static bool IsPlaying()
        {
            // !!! add conditions: IsDead, IsLoading
            return IsReplayFolderExists();
        }

        public static bool IsReplayFolderExists()
        {
            return Directory.Exists(hotsTempFolderPath);
        }

        // !!! IsDeath
        // !!! IsLoading
    }
}
