﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace HotsMinimapInstructor.Control
{
    public static class ClipImage
    {
        public static Bitmap Capture(Rectangle rectangle)
        {
            var screenBmp = new Bitmap(rectangle.Width, rectangle.Height, PixelFormat.Format32bppArgb);
            using (var bmpGraphics = Graphics.FromImage(screenBmp))
            {
                bmpGraphics.CopyFromScreen(rectangle.X, rectangle.Y, 0, 0, screenBmp.Size);
                return screenBmp;
            }
        }
    }
}
