﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tobii.StreamEngine;

namespace HotsMinimapInstructor
{
    class EyeTracking
    {
        public class PointEventArgs : EventArgs
        {
            public float x;
            public float y;

            public PointEventArgs(float x, float y)
            {
                this.x = x;
                this.y = y;
            }
        }

        public event EventHandler<PointEventArgs> OnGazePointUpdate;


        private Thread thread;
        private bool isUpdating;


        private void CheckError(tobii_error_t error)
        {
            if (error != tobii_error_t.TOBII_ERROR_NO_ERROR)
            {
                throw new Exception(Interop.tobii_error_message(error));
            }
        }

        public void Start()
        {
            isUpdating = true;
            thread = new Thread(new ThreadStart(Update));
            thread.IsBackground = true;
            thread.Start();
        }

        public void Stop()
        {
            isUpdating = false;
            thread.Join();
        }

        private void Update()
        {
            // 初期化
            var error = Interop.tobii_api_create(out IntPtr api, null);
            CheckError(error);

            // デバイスリストの取得
            error = Interop.tobii_enumerate_local_device_urls(api, out List<string> deviceNames);
            CheckError(error);

            // デバイスハンドラの作成
            error = Interop.tobii_device_create(api, deviceNames[0], out IntPtr device);
            CheckError(error);

            // エンジンの作成
            error = Interop.tobii_engine_create(api, out IntPtr engine);
            CheckError(error);

            // 視線の取得
            error = Interop.tobii_gaze_point_subscribe(device, GazePointCallback);
            CheckError(error);

            var gazeDevices = new IntPtr[] { device };

            while (isUpdating)
            {
                error = Interop.tobii_wait_for_callbacks(engine, gazeDevices);
                CheckError(error);
                if ((error != tobii_error_t.TOBII_ERROR_NO_ERROR) && (error != tobii_error_t.TOBII_ERROR_TIMED_OUT))
                {
                    CheckError(error);
                }

                error = Interop.tobii_device_process_callbacks(device);
                CheckError(error);

                Thread.Sleep(1);
            }

            // エンジンの破棄
            error = Interop.tobii_engine_destroy(engine);
            CheckError(error);

            // デバイスハンドラの破棄
            error = Interop.tobii_device_destroy(device);
            CheckError(error);

            // 終了処理
            error = Interop.tobii_api_destroy(api);
            CheckError(error);
        }

        private void GazePointCallback(ref tobii_gaze_point_t gazePoint)
        {
            if (gazePoint.validity == tobii_validity_t.TOBII_VALIDITY_VALID)
            {
                OnGazePointUpdate?.Invoke(this, new PointEventArgs(gazePoint.position.x, gazePoint.position.y));
            }
        }
    }
}
