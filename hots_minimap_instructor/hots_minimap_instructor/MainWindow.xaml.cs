﻿using HotsMinimapInstructor.Control;
using HotsMinimapInstructor.View;
using HotsTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HotsMinimapInstructor
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private enum State : int
        {
            NoGame = 0,
            ReplayFolderDetected = 1,
            Loading = 2,
            Playing = 3,
            IsDead = 4,
        }

        private const int ReplayFolderCheckInterval = 5000;
        private const int NoCheckMsec = 5000;

        private ClipSettingWindow clipSettingWindow;
        private SoundSettingWindow soundSettingWindow;

        private EyeTracking eyeTracking = new EyeTracking();
        private BackgroundWorker replayFolderChecker;
        private BackgroundWorker captureImagesWorker;
        private BackgroundWorker playWarningSoundWorker;

        private SoundPlayer warningSound;
        private SoundPlayer okSound;

        private State state = State.NoGame;
        private System.Windows.Size screenSize;


        private bool IsCounting { get; set; }
        private bool IsAlive { get; set; }

        private bool IsLookingMinimap { get; set; }
        private Settings settings;


        public MainWindow()
        {
            InitializeComponent();

            settings = LoadSetting();
            LoadSounds();

            clipSettingWindow = new ClipSettingWindow(settings);
            soundSettingWindow = new SoundSettingWindow(settings);

            screenSize = SystemParameters.WorkArea.Size;
            eyeTracking.OnGazePointUpdate += EyeTracking_OnGazePointUpdate;
            eyeTracking.Start();
            InitializeTimers();

            UpdateTitle("No game");
            replayFolderChecker.RunWorkerAsync();
        }

        private Settings LoadSetting()
        {
            try
            {
                return XmlAccessor.Load(typeof(Settings), Constants.SettingsFilePath) as Settings;
            }
            catch (Exception)
            {
                // !!!
            }
            return new Settings();
        }

        private void LoadSounds()
        {
            try
            {
                if (File.Exists(settings.WarningSoundPath))
                {
                    warningSound = new SoundPlayer(settings.WarningSoundPath);
                }
                if (File.Exists(settings.OkSoundPath))
                {
                    okSound = new SoundPlayer(settings.OkSoundPath);
                }
            }
            catch (Exception)
            {
                // !!!
            }
        }

        private void EyeTracking_OnGazePointUpdate(object sender, EyeTracking.PointEventArgs e)
        {
            double x = e.x * screenSize.Width;
            double y = e.y * screenSize.Height;
            IsLookingMinimap = ((x > 1400) && (x < screenSize.Width) && (y > 780)) ? true : false;
        }

        private void InitializeTimers()
        {
            replayFolderChecker = new BackgroundWorker();
            replayFolderChecker.DoWork += ReplayFolderChecker_DoWork;
            replayFolderChecker.ProgressChanged += ReplayFolderChecker_ProgressChanged;
            replayFolderChecker.WorkerReportsProgress = true;
            replayFolderChecker.WorkerSupportsCancellation = true;

            captureImagesWorker = new BackgroundWorker();
            captureImagesWorker.DoWork += CaptureImagesWorker_DoWork;
            captureImagesWorker.WorkerReportsProgress = true;
            captureImagesWorker.WorkerSupportsCancellation = true;

            playWarningSoundWorker = new BackgroundWorker();
            playWarningSoundWorker.DoWork += UpdateWorker_DoWork;
            playWarningSoundWorker.ProgressChanged += PlayWarningSoundWorker_ProgressChanged;
            playWarningSoundWorker.WorkerReportsProgress = true;
            playWarningSoundWorker.WorkerSupportsCancellation = true;
        }

        private void CaptureImagesWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!captureImagesWorker.CancellationPending)
            {
                var image = ClipImage.Capture(settings.CounterClipArea);
                int x = ((image.Width - 1) / 2) + 1;
                var topColor = image.GetPixel(x, 0);
                var bottomColor = image.GetPixel(x, image.Height - 1);
                IsCounting = (IsWhite(topColor) && IsWhite(bottomColor)) ? true : false;

                clipSettingWindow.UpdateCapturedImage(image);
            }
        }

        private bool IsWhite(System.Drawing.Color color)
        {
            return ((color.R > 200) && (color.G > 200) && (color.B > 200)) ? true : false;
        }

        private void UpdateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int nextAudioPlayMsec = NoCheckMsec;
            var noCheckElapsed = new System.Diagnostics.Stopwatch();
            noCheckElapsed.Start();

            var previousGameState = state;
            while (!playWarningSoundWorker.CancellationPending)
            {
                System.Threading.Thread.Sleep(20);

                if (!IsCounting)
                {
                    // カウント中でなければゲーム中とみなさない。
                    noCheckElapsed.Restart();
                    nextAudioPlayMsec = NoCheckMsec;
                }
                else if (IsLookingMinimap)
                {
                    if (nextAudioPlayMsec != NoCheckMsec)
                    {
                        if (okSound != null)
                        {
                            okSound.Play();
                        }
                    }

                    noCheckElapsed.Restart();
                    nextAudioPlayMsec = NoCheckMsec;
                }
                else if (noCheckElapsed.ElapsedMilliseconds > nextAudioPlayMsec)
                {
                    if (warningSound != null)
                    {
                        warningSound.Play();
                    }
                    nextAudioPlayMsec += 2000;
                }
            }
        }

        private void PlayWarningSoundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            state = EnterState((State)e.ProgressPercentage);
        }

        private void ReplayFolderChecker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool previousDetected = false;
            while (!replayFolderChecker.CancellationPending)
            {
                bool currentDetected = GameDetector.IsReplayFolderExists();
                if (currentDetected != previousDetected)
                {
                    var state = currentDetected ? State.ReplayFolderDetected : State.NoGame;
                    replayFolderChecker.ReportProgress((int)state);
                    previousDetected = currentDetected;
                }

                System.Threading.Thread.Sleep(ReplayFolderCheckInterval);
            }
        }

        private void ReplayFolderChecker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            state = EnterState((State)e.ProgressPercentage);
        }

        private State EnterState(State nextState)
        {
            switch (nextState)
            {
                case State.NoGame:
                    captureImagesWorker.CancelAsync();
                    playWarningSoundWorker.CancelAsync();
                    // !!! stop screen capture
                    UpdateTitle("No game");
                    break;

                case State.ReplayFolderDetected:
                    captureImagesWorker.RunWorkerAsync();
                    playWarningSoundWorker.RunWorkerAsync();
                    UpdateTitle("Replay folder detected");
                    return State.Loading;

                case State.Loading:
                    // !!! when detect counter colon
                    UpdateTitle("Loading");
                    break;

                case State.Playing:
                    // !!! when detect counter colon
                    UpdateTitle("Playing");
                    break;

                case State.IsDead:
                    // !!! when detect skeleton icon
                    UpdateTitle("Is dead");
                    break;
            }

            return nextState;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            soundSettingWindow.Quit();
            clipSettingWindow.Quit();
            e.Cancel = false;
        }

        public void UpdateTitle(string text)
        {
            Title = text;
        }

        private void SoundIcon_Click(object sender, RoutedEventArgs e)
        {
            soundSettingWindow.Show();
        }

        private void ClipIcon_Click(object sender, RoutedEventArgs e)
        {
            clipSettingWindow.Show();
        }
    }
}
