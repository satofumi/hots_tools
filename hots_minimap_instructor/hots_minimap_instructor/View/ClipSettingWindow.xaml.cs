﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HotsMinimapInstructor.View
{
    /// <summary>
    /// ClipAreaAdjustWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ClipSettingWindow : Window
    {
        private bool closeCancel = true;
        private Settings settings;


        public ClipSettingWindow(Settings settings)
        {
            InitializeComponent();
            this.settings = settings;
        }

        public void UpdateCapturedImage(System.Drawing.Bitmap image)
        {
            Dispatcher.BeginInvoke(
             new Action(() =>
             {
                 CapturedImage.Height = image.Height * 8;
                 CapturedImage.Width = image.Width * 8;
                 CapturedImage.Source = Convert(image);
             }));
        }

        private BitmapImage Convert(System.Drawing.Bitmap image)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                image.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        private void LeftButton_Click(object sender, RoutedEventArgs e)
        {
            var rect = settings.CounterClipArea;
            settings.CounterClipArea = new System.Drawing.Rectangle(rect.X - 1, rect.Y, rect.Width, rect.Height);
        }

        private void RightButton_Click(object sender, RoutedEventArgs e)
        {
            var rect = settings.CounterClipArea;
            settings.CounterClipArea = new System.Drawing.Rectangle(rect.X + 1, rect.Y, rect.Width, rect.Height);
        }

        private void UpButton_Click(object sender, RoutedEventArgs e)
        {
            var rect = settings.CounterClipArea;
            settings.CounterClipArea = new System.Drawing.Rectangle(rect.X, rect.Y - 1, rect.Width, rect.Height);
        }

        private void DownButton_Click(object sender, RoutedEventArgs e)
        {
            var rect = settings.CounterClipArea;
            settings.CounterClipArea = new System.Drawing.Rectangle(rect.X, rect.Y + 1, rect.Width, rect.Height);
        }

        private void ZoomInButton_Click(object sender, RoutedEventArgs e)
        {
            var rect = settings.CounterClipArea;
            settings.CounterClipArea = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width, rect.Height + 1);
        }

        private void ZoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            var rect = settings.CounterClipArea;
            settings.CounterClipArea = new System.Drawing.Rectangle(rect.X, rect.Y, rect.Width, rect.Height - 1);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = closeCancel;
            Visibility = Visibility.Collapsed;
        }

        public void Quit()
        {
            closeCancel = false;
            Close();
        }
    }
}
