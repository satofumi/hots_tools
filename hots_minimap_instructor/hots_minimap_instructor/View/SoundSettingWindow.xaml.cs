﻿using HotsMinimapInstructor.Control;
using HotsTools;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HotsMinimapInstructor.View
{
    /// <summary>
    /// SoundSettingWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class SoundSettingWindow : Window
    {
        public static RoutedCommand WarningPathCommand = new RoutedCommand();
        public static RoutedCommand OkPathCommand = new RoutedCommand();

        private bool closeCancel = true;
        private Settings settings;


        public SoundSettingWindow(Settings settings)
        {
            InitializeComponent();
            this.settings = settings;

            WarningPathLabel.Text = settings.WarningSoundPath;
            OkPathLabel.Text = settings.OkSoundPath;
        }

        private string OpenFileDialog()
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "wav|*.wav";
            if (dialog.ShowDialog() == false)
            {
                return null;
            }

            return dialog.FileName;
        }

        private void WarningPathCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string filePath = OpenFileDialog();
            if (!string.IsNullOrEmpty(filePath))
            {
                settings.WarningSoundPath = filePath;
                WarningPathLabel.Text = filePath;
            }
        }

        private void OkPathCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string filePath = OpenFileDialog();
            if (!string.IsNullOrEmpty(filePath))
            {
                settings.OkSoundPath = filePath;
                OkPathLabel.Text = filePath;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                XmlAccessor.Save(typeof(Settings), Constants.SettingsFilePath, settings);
            }
            catch (Exception)
            {
            }

            e.Cancel = closeCancel;
            Visibility = Visibility.Collapsed;
        }

        public void Quit()
        {
            closeCancel = false;
            Close();
        }
    }
}
