﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotsMinimapInstructor
{
    public class Settings
    {
        public string WarningSoundPath { get; set; }
        public string OkSoundPath { get; set; }

        public System.Drawing.Rectangle CounterClipArea { get; set; }

        public Settings()
        {
            WarningSoundPath = "Audio/warning.wav";
            OkSoundPath = "Audio/ok.wav";
            CounterClipArea = new System.Drawing.Rectangle(960, 0, 5, 19);
        }
    }
}
