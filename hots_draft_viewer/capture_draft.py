# -*- coding: utf-8 -*-
from PIL import ImageGrab
from PIL import Image


# 枠線までのピクセル長を取得する
def find_border(img, max_length, point, step):
    x, y = point
    xx, yy = step
    for i in range(max_length):
        r, g, b = img.getpixel((x + (xx * i), y + (yy * i)))
        if r != 0xff and g != 0xff and b != 0xff:
            return i
    return max_length


# メインディスプレイのスクリーンショットを撮る
img = ImageGrab.grab()

# 上の余白サイズを取得する
max_length = img.height - 1
point = (img.width / 2, 150)
step = (0, +1)
top_margin = find_border(img, max_length, point, step) + 150 - 30

# 下の余白サイズを取得する
point = (img.width / 2, img.height - 150)
step = (0, -1)
bottom_margin = find_border(img, max_length, point, step) + 150

# 左の余白サイズを取得する
draft_image_y = 350
point = (0, draft_image_y)
step = (+1, 0)
left_margin = find_border(img, max_length, point, step)

# 右の余白サイズを取得する
point = (img.width - 100, draft_image_y)
step = (-1, 0)
right_margin = find_border(img, max_length, point, step) + 100

# 余白を切り取った画像を作成する
offset = 10
clip_rect = (left_margin - offset, top_margin - offset, img.width - right_margin + offset, img.height - bottom_margin + offset)
clipped = img.crop(clip_rect)
clipped.save('clipped.png')
