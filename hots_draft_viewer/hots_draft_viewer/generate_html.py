# -*- coding: utf-8 -*-
import sys
import codecs
import binascii
import yaml
from . import chat_from_replay
from . import translate_name

Actual_icon_dir = 'hots_replay_chat/static/icons'


def _normalized_name(name):
    return name.replace("'", '').replace('ú', 'u')

def _icon_name(name, icon_path_prefix):
    english_name = translate_name.translate_name.convert_to_english(name)
    icon_file = _normalized_name(english_name) + '.png'
    return icon_path_prefix + 'icons/' + icon_file

def _print_killed_line(tokens, icon_path_prefix):
    time = tokens[0].strip()
    name = tokens[2].strip()
    dead_count = tokens[3].strip()
    team = tokens[4].strip()
    border_color = 'blue' if team == 'Ally' else 'red'

    output = ''
    output += '<span class="dead">'
    output += '<img src="' + _icon_name(name, icon_path_prefix) + '" style="width: 38px; height: 38px; margin-left: 1em; margin-top: 1px; margin-right: 4px; opacity: 0.6; border: 2px ' + border_color + ' solid;" align="left">\n'
    output += '<span style="color: gray; font-size: small">' + time + '</span>\n'
    output += '<span style="color: gray; font-size: small">' + name + '</span>\n'
    output += '<br>\n'
    output += '<span style="color: gray">Dead (total ' + dead_count + ' times)</span>\n'
    output += '<br clear="left">\n'
    output += '</span>'

    return output

def _print_message_line(tokens, icon_path_prefix):
    time = tokens[0].strip()
    name = tokens[2].strip()
    message = tokens[3].strip().strip("'")

    output = ''
    output += '<img src="' + _icon_name(name, icon_path_prefix) + '" align="left" style="width: 42px; height: 42px; margin-top: 1px; margin-right: 4px; border: 2px blue solid;">\n'
    output += '<span style="color: gray; font-size: small">' + time + '</span>\n'
    output += '<span style="color: gray; font-size: small">' + name + '</span>\n'
    output += '<br>\n'
    output += '<span>' + message + '</span>\n'
    output += '<br clear="left">\n'

    return output


def generate_html(yaml_text, icon_path_prefix):
    output = ''

    text = yaml_text.replace('\x0e', '').replace('\x0f', '')
    data = yaml.safe_load(text)

    output += '<div class="messages">\n'
    for line in data['Messages']:
        tokens = line.split(',')
        message_type = tokens[1].strip()
        if message_type == 'killed':
            output += _print_killed_line(tokens, icon_path_prefix)
        else:
            output += _print_message_line(line.split(',', 3), icon_path_prefix)
    output += '</div>\n'

    return output


if __name__ == '__main__':
    replay_file = sys.argv[1]
    yaml_text = chat_from_replay.output_yaml(replay_file)
    html_text = generate_html(yaml_text, '')

    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    print(html_text)
