# -*- coding: utf-8 -*-
import os
if os.name == 'nt':
    from win32com.shell import shell, shellcon
import fnmatch
import datetime


class Replay_file:
    def __init__(self, file_path):
        self.file_path = file_path

    def encoded_path(self):
        return self.file_path.replace(' ', '%20').replace('/', '%2F').replace(':', '%3A').replace('(', '%28').replace(')', '%29').replace("'", '%27')

    def file_name(self):
        return os.path.basename(self.file_path)

    def mtime(self):
        return os.path.getmtime(self.file_path)

    def date_time(self):
        return datetime.datetime.fromtimestamp(self.mtime()).strftime('%Y-%m-%d %H:%M')

    @staticmethod
    def _path_join(folder_path, file_name):
        return os.path.join(folder_path, file_name).replace('\\', '/')


    @staticmethod
    def _replay_folders():
        if os.name != 'nt':
            return set()

        document_folder = shell.SHGetFolderPath(0, shellcon.CSIDL_PERSONAL, None, 0)
        hots_folder = Replay_file._path_join(document_folder, 'Heroes of the Storm/Accounts/')

        folders = set()
        for root, dirnames, filenames in os.walk(hots_folder):
            for filename in fnmatch.filter(filenames, '*.StormReplay'):
                folder_path = os.path.dirname(Replay_file._path_join(root, filename))
                folders.add(folder_path)
                break
        return folders


    @staticmethod
    def files():
        replay_folders = Replay_file._replay_folders()
        if len(replay_folders) == 0:
            return []

        replay_files = []
        for folder_path in replay_folders:
            for replay_file in os.listdir(folder_path):
                if replay_file.endswith('.StormReplay'):
                    file_path = Replay_file._path_join(folder_path, replay_file)
                    replay_files.append(Replay_file(file_path))
        replay_files.sort(key=lambda x: x.mtime(), reverse=True)
        return replay_files
