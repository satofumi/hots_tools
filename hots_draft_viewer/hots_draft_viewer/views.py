from pyramid.view import view_config

import os
import uuid
import shutil
from . import replay_file
from . import draft_from_replay
from . import chat_from_replay
from . import generate_html


def _path_join(folder_path, file_name):
    return os.path.join(folder_path, file_name).replace('\\', '/')


@view_config(route_name='replay_files',
             renderer='templates/replay_files.jinja2')
def replay_files(request):
    files = replay_file.Replay_file.files()
    if len(files) == 0:
        return {'error': 'not found replay files.'}

    return {
        'replay_files': files,
    }


@view_config(route_name='show_draft', renderer='templates/show_draft.jinja2')
def show_draft(request):
    try:
        # upload server
        if request.POST.has_key('StormReplay'):
            replay_file = _uploaded_file(request.POST['StormReplay'])
            draft_html, chat_html = _parse_replay_file(replay_file)
            try:
                replay_file_name = request.POST['StormReplay'].filename
            finally:
                # remove uploaded file
                os.remove(replay_file)
        else:
            # local server
            replay_file = '/'.join(request.matchdict['replay_file'])
            draft_html, chat_html = _parse_replay_file(replay_file)
            replay_file_name = os.path.basename(replay_file)

        return {
            'replay_file_name': replay_file_name,
            'draft_html': draft_html,
            'chat_html': chat_html,
        }
    except:
        import traceback
        return {'error': 'Invalid file: ' + replay_file + '\n' + traceback.format_exc()}


def _parse_replay_file(replay_file):
    draft_yaml = draft_from_replay.yaml_text(replay_file)
    draft_html = draft_from_replay.html_text(draft_yaml, '/static/')
    chat_yaml = chat_from_replay.output_yaml(replay_file)
    chat_html = generate_html.generate_html(chat_yaml, '/static/')

    return [draft_html, chat_html]


def _uploaded_file(replay):
    input_file = replay.file
    uploaded_file = _path_join('tmp/', '%s.StormReplay' % uuid.uuid4())
    input_file.seek(0)
    with open(uploaded_file, 'wb') as output_file:
        shutil.copyfileobj(input_file, output_file)

    return uploaded_file


@view_config(route_name='upload_file',
             renderer='templates/upload_replay.jinja2')
def upload_file(request):
    return {}
