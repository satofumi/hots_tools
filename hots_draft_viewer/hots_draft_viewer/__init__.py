from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('replay_files', '/')
    config.add_route('show_draft', '/show/draft/*replay_file')
    config.add_route('upload_file', '/upload/replay')
    config.scan()
    return config.make_wsgi_app()
