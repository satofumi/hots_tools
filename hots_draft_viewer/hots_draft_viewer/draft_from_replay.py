# -*- coding: utf-8 -*-
import sys

import os
import datetime
import yaml

import mpyq
from heroprotocol import versions

from . import translate_name
#import translate_name


def _strip_non_alphabet(name):
    fixed_name = name
    if isinstance(name, (bytes, bytearray)):
        fixed_name = name.decode()
    return fixed_name.replace("'", '').replace('ú', 'u')

def _icon_img(name, additional_class, icon_path_prefix):
    icon_name = _strip_non_alphabet(name)
    icon_name = translate_name.translate_name.convert_to_english(icon_name)
    icon_name = translate_name.translate_name.tag_to_name(icon_name)
    if os.path.exists('hots_draft_viewer/static/icons/' + icon_name + '.png'):
        return '<img class="hero' + additional_class + '" src="/static/icons/' + icon_name + '.png">'
    else:
        return ' ' + icon_name + ' '

def _hero_icon(draft, players, icon_path_prefix):
    if draft['_event'] == 'NNet.Replay.Tracker.SHeroBannedEvent':
        return _icon_img(draft['m_hero'].decode(), ' ban', icon_path_prefix)
    else:
        player_id = int(draft['m_controllingPlayer'])
        player = players[player_id]
        return _icon_img(player['hero'], '', icon_path_prefix)


def yaml_text(replay_file):
    archive = mpyq.MPQArchive(replay_file)
    contents = archive.header['user_data_header']['content']
    header = versions.latest().decode_replay_header(contents)
    baseBuild = header['m_version']['m_baseBuild']
    try:
        protocol = versions.build(baseBuild)
    except:
        print >> sys.stderr, 'Unsupported base build: %d' % baseBuild
        sys.exit(1)


    # ゲームタイプの読み出し
    contents = archive.read_file('replay.initData')
    initdata = protocol.decode_replay_initdata(contents)
    game_type_value = initdata['m_syncLobbyState']['m_gameDescription']['m_gameOptions']['m_ammId']

    game_type = ''
    has_draft = False
    if game_type_value == 50001:
        game_type = 'Quick Match'
    elif game_type_value == 50021:
        game_type = 'AI Coop'
    elif game_type_value == 50031:
        game_type = 'Brawl'
    elif game_type_value == 50051:
        game_type = 'Unranked Draft'
    elif game_type_value == 50061:
        game_type = 'Hero League'
    elif game_type_value == 50071:
        game_type = 'Team League'
    elif game_type_value == 50091:
        game_type = 'Storm League'
    else:
        game_type = 'Custom'


    # ドラフト情報の読み出し
    contents = archive.read_file('replay.tracker.events')
    events = protocol.decode_replay_tracker_events(contents)
    raw_draft_events = []
    raw_result_data = {}
    has_draft = False
    ban_3 = False
    above_hero_name = None
    for event in events:
        event_name = event['_event']
        if event_name == 'NNet.Replay.Tracker.SHeroBannedEvent' or event_name == 'NNet.Replay.Tracker.SHeroPickedEvent':
            raw_draft_events.append(event)
            has_draft = True

        elif 'm_eventName' in event and event['m_eventName'] == b'EndOfGameTalentChoices':
            player_id = int(event['m_intData'][0]['m_value'])
            raw_result_data[player_id] = event

    if len(raw_draft_events) == 16:
        ban_3 = True


    # マップ名、プレイヤー情報の読み出し
    contents = archive.read_file('replay.details')
    details = protocol.decode_replay_details(contents)
    map_name = details['m_title']
    raw_players = {}
    for player in details['m_playerList']:
        slot_id = player['m_workingSetSlotId']
        raw_players[slot_id] = player

    m_timeUTC = int(details['m_timeUTC'])
    game_datetime = datetime.datetime.fromtimestamp((m_timeUTC - 116444736000000000) // 10000000)

    if has_draft:
        above_hero_name = raw_draft_events[4]['m_hero']
    else:
        above_hero_name = raw_players[0]['m_hero'].decode()


    # ユーザ情報の統合
    players = {}
    for k, v in raw_players.items():
        player = {
            'team': v['m_teamId'],
            'name': v['m_name'].decode(),
            'hero': v['m_hero'].decode(),
        }
        players[k] = player


    # 勝敗の取得
    above_team_id = 0
    if has_draft:
        above_team_id = int(raw_draft_events[0]['m_controllingTeam']) - 1
    team_results = [None, None]

    result_hero_name = 'Hero' + _strip_non_alphabet(translate_name.translate_name.name_to_tag(above_hero_name))
    for k,v in raw_result_data.items():
        if result_hero_name == v['m_stringData'][0]['m_value'].decode():
            team_results[0] = v['m_stringData'][1]['m_value'].decode()
    team_results[1] = 'Win' if team_results[0] == 'Loss' else 'Loss'

    data = {}
    data['game_datetime'] = game_datetime
    data['game_type'] = game_type
    data['map_name'] = map_name
    data['has_draft'] = has_draft
    data['ban_3'] = ban_3
    data['drafts'] = raw_draft_events
    data['above_team_id'] = above_team_id
    data['players'] = players
    data['team_results'] = team_results

    return yaml.safe_dump(data, allow_unicode=True)


def html_text(yaml_text, icon_path_prefix):
    data = yaml.safe_load(yaml_text)

    game_datetime = data['game_datetime']
    game_type = data['game_type']
    map_name = data['map_name']
    has_draft = data['has_draft']
    ban_3 = data['ban_3']
    drafts = data['drafts']
    above_team_id = data['above_team_id']
    players = data['players']
    team_results = data['team_results']

    # プレイ日時、マップ名の整形
    output = ''
    output += '<span class="date">' + str(game_datetime) + '</span><br>'
    output += '<span class="game_type">' + game_type + '</span>'
    output += '<h3>' + map_name.decode() + '</h3>'

    # BAN 情報の整形
    above_team = 'team1' if above_team_id == 0 else 'team2'
    below_team = 'team1' if above_team_id != 0 else 'team2'
    if ban_3:
        output += '<table class="draft"><tr><th class="draft_team">&nbsp;</th><th class="select" colspan="2">1st BAN</th><th class="select" colspan="2">2nd BAN</th><th class="select" colspan="3">Pick</th><th class="select" colspan="2">3rd BAN</th><th class="select" colspan="3">Pick</th></tr>'

        output += '<tr class="{}"><td class="draft_team">Team{}</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td>&nbsp;</td></tr>'.format(above_team, above_team_id + 1, _hero_icon(drafts[0], players, icon_path_prefix), _hero_icon(drafts[2], players, icon_path_prefix), _hero_icon(drafts[4], players, icon_path_prefix), _hero_icon(drafts[7], players, icon_path_prefix) + _hero_icon(drafts[8], players, icon_path_prefix), _hero_icon(drafts[10], players, icon_path_prefix), _hero_icon(drafts[13], players, icon_path_prefix) + _hero_icon(drafts[14], players, icon_path_prefix))

        output += ('<tr class="{}"><td class="draft_team">Team{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td class="select">&nbsp;</td><td class="select">{}</td></tr></table>').format(below_team, (1 - above_team_id) + 1, _hero_icon(drafts[1], players, icon_path_prefix), _hero_icon(drafts[3], players, icon_path_prefix), _hero_icon(drafts[5], players, icon_path_prefix) + _hero_icon(drafts[6], players, icon_path_prefix), _hero_icon(drafts[9], players, icon_path_prefix), _hero_icon(drafts[11], players, icon_path_prefix) + _hero_icon(drafts[12], players, icon_path_prefix), _hero_icon(drafts[15], players, icon_path_prefix))
        output += '<br>'

    elif has_draft:
        output += '<table class="draft"><tr><th class="draft_team">&nbsp;</th><th class="select" colspan="2">1st BAN</th><th class="select" colspan="3">Pick</th><th class="select" colspan="2">2nd BAN</th><th class="select" colspan="3">Pick</th></tr>'

        output += '<tr class="{}"><td class="draft_team">Team{}</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td>&nbsp;</td></tr>'.format(above_team, above_team_id + 1, _hero_icon(drafts[0], players, icon_path_prefix), _hero_icon(drafts[4], players, icon_path_prefix), _hero_icon(drafts[7], players, icon_path_prefix) +_hero_icon(drafts[8], players, icon_path_prefix), _hero_icon(drafts[3], players, icon_path_prefix), _hero_icon(drafts[11], players, icon_path_prefix) +_hero_icon(drafts[12], players, icon_path_prefix))

        output += ('<tr class="{}"><td class="draft_team">Team{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td class="select">&nbsp;</td><td class="select">{}</td></td class="select"></tr></table>').format(below_team, (1 - above_team_id) + 1, _hero_icon(drafts[1], players, icon_path_prefix), _hero_icon(drafts[5], players, icon_path_prefix) +_hero_icon(drafts[6], players, icon_path_prefix), _hero_icon(drafts[2], players, icon_path_prefix), _hero_icon(drafts[9], players, icon_path_prefix) +_hero_icon(drafts[10], players, icon_path_prefix), _hero_icon(drafts[13], players, icon_path_prefix))
        output += '<br>'

    output += '<table class="teams">'
    output += '<tr><th class="result">' + team_results[0] + '</th><td>&nbsp;</td><th class="result">' + team_results[1] + '</th></tr>'
    output += '<tr><td class="' + above_team + '">'

    for k, player in players.items():
        if player['team'] == above_team_id:
            output += _icon_img(player['hero'], ' small', icon_path_prefix) + '&nbsp;' + player['name'] + '<br>'

    output += '</td><th class="vs">VS</th><td class="' + below_team + '">'

    for k, player in players.items():
        if player['team'] == (1 - above_team_id):
            output += _icon_img(player['hero'], ' small', icon_path_prefix) + '&nbsp;' + player['name'] + '<br>'

    output += '</td></tr></table>'
    return output


if __name__ == '__main__':
    replay_file = sys.argv[1]
    yaml_output = yaml_text(replay_file)
    print(yaml_output)
    #html_output = html_text(yaml_output, '/static/')
    #print(html_output)
