import sys

import mpyq
from heroprotocol import versions


_team_names = ['Ally', 'Enemy']
_player_ids = []

def _team_side(id):
    return _team_names[0] if id <= 4 else _team_names[1]

def _to_mmss(key):
    total_seconds = key / 16
    minutes = int(total_seconds / 60)
    seconds = int(total_seconds % 60)
    return '{:02d}:{:02d}'.format(minutes, seconds) + ', '

def output_yaml(replay_file):
    global _team_names
    _team_names = ['Ally', 'Enemy']

    archive = mpyq.MPQArchive(replay_file)
    contents = archive.header['user_data_header']['content']
    header = versions.latest().decode_replay_header(contents)
    baseBuild = header['m_version']['m_baseBuild']
    try:
        protocol = versions.build(baseBuild)
    except:
        print >> sys.stderr, 'Unsupported base build: %d' % baseBuild
        sys.exit(1)

    lower_index_is_ally = True

    contents = archive.read_file('replay.details')
    details = protocol.decode_replay_details(contents)
    players = {}
    for player in details['m_playerList']:
        player_id = player['m_workingSetSlotId']
        _player_ids.append(player_id)
        players[player_id] = player['m_hero']
    contents = archive.read_file('replay.message.events')
    chat_messages = {}
    for event in protocol.decode_replay_message_events(contents):
        if event['_event'] == 'NNet.Game.SChatMessage':
            player_id = event['_userid']['m_userId']
            if player_id in players:
                player = players[player_id]
            else:
                player = 'Observer'

            message = event['m_string'].decode().replace(':', ';')
            message = 'message, ' + player.decode() + ", '" + message + "'"
            game_loop = event['_gameloop']
            chat_messages[game_loop] = message

            if player_id >= _player_ids[5]:
                lower_index_is_ally = False

    if not lower_index_is_ally:
        _team_names[0], _team_names[1] = _team_names[1], _team_names[0]


    contents = archive.read_file('replay.tracker.events')
    killed_counts = [0] * 20
    killed_messages = {}
    for event in protocol.decode_replay_tracker_events(contents):
        if 'm_eventName' in event and event['m_eventName'] == 'PlayerDeath':
            killed_player_id = _player_ids[event['m_intData'][0]['m_value'] - 1]
            killed_counts[killed_player_id] += 1
            message = 'killed, ' + players.get(killed_player_id, 'a') + ', ' + str(killed_counts[killed_player_id]) + ', ' + _team_side(killed_player_id)
            game_loop = event['_gameloop']
            killed_messages[game_loop] = message


    output = ''
    output += 'TotalKilled:\n'
    for index in range(len(players)):
        if index == 0 or index == 5:
            output += '  ' + _team_side(_player_ids[index]) + ':\n'

        message = '    - ' + players.get(_player_ids[index], 'a').decode() + ': ' + str(killed_counts[_player_ids[index]])
        output += message + '\n'

    output += '\n'
    output += 'Messages:\n'

    chat_messages.update(killed_messages)
    for key in sorted(chat_messages.keys()):
        output += '  - ' + _to_mmss(key) + chat_messages[key] + '\n'

    return output


if __name__ == '__main__':
    replay_file = sys.argv[1]
    text = output_yaml(replay_file)
    print(text)
