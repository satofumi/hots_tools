Dim wshShell
Set wshShell = CreateObject("WScript.Shell")

Dim wshEnv
Set wshEnv = wshShell.Environment("Process")

wshEnv.Item("PATH") = wshShell.ExpandEnvironmentStrings("C:\Python27;C:\Python27\Scripts") & ";" & wshShell.ExpandEnvironmentStrings("%PATH%")

wshShell.Run "C:\Python27\python -m pip install virtualenvwrapper-win", 1, True
Dim fso
Set fso = CreateObject("Scripting.FileSystemObject")

wshShell.Run "mkvirtualenv hots", 1, True
Dim fromFile, toFile
fromFile = fso.GetParentFolderName(WScript.ScriptFullName) + "\pywin32.pth"
toFile = wshShell.ExpandEnvironmentStrings("%UserProfile%\Envs\hots\Lib\site-packages\pywin32.pth")
Call fso.CopyFile(fromFile, toFile)

wshShell.Run "workon hots & python setup.py develop", 1, True
