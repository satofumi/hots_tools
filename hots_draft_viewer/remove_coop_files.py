import sys
sys.path.append('hots_draft_viewer')
sys.path.append('heroprotocol')

import os
from replay_file import Replay_file
from mpyq import mpyq
import protocol29406


def replay_game_type(replay_file):
    archive = mpyq.MPQArchive(replay_file)
    contents = archive.header['user_data_header']['content']
    header = protocol29406.decode_replay_header(contents)
    baseBuild = header['m_version']['m_baseBuild']
    try:
        protocol = __import__('protocol%s' % (baseBuild,))
    except:
        print >> sys.stderr, 'Unsupported base build: %d' % baseBuild
        sys.exit(1)


    contents = archive.read_file('replay.initData')
    initdata = protocol.decode_replay_initdata(contents)
    game_type_value = initdata['m_syncLobbyState']['m_gameDescription']['m_gameOptions']['m_ammId']

    game_type = ''
    has_draft = False
    if game_type_value == 50001:
        game_type = 'Quick Match'
    elif game_type_value == 50021:
        game_type = 'AI Coop'
    elif game_type_value == 50051:
        game_type = 'Unranked Draft'
    elif game_type_value == 50061:
        game_type = 'Hero League'
    elif game_type_value == 50071:
        game_type = 'Team League'
    else:
        game_type = 'Custom'

    return game_type


for replay_file in Replay_file.files():
    game_type = replay_game_type(replay_file.file_path)
    if game_type == 'AI Coop':
        print(replay_file.file_path)
        os.remove(replay_file.file_path)
