# HotS Draft Viewer

Heroes of the Storm のリプレイファイルからドラフト情報を表示するツールです。

 - リプレイファイルがあれば、ドラフトを後から確認できます。


## ライセンス

 - MIT ライセンス


## インストール手順
 - 依存ツールのインストール
   - https://bitbucket.org/satofumi/hots_tools/downloads/python-2.7.14.amd64.msi
   - https://bitbucket.org/satofumi/hots_tools/downloads/Git-2.14.2.2-64-bit.exe

 - 初期化
   - パッケージを展開する。
   - initialize.vbs を実行する。

 - 起動
   - start.vbs を実行する。


## 使い方

 - 起動してから http://localhost:6128 にアクセスする。


## 連絡先

バグや要望がありましたら、開発サイトの課題トラッカーにお知らせ下さい。

https://bitbucket.org/satofumi/hots_tools/issues
