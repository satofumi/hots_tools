/*!
  \page storm_replay_page StormReplay ファイルについて

  heroprotocol にて得られる情報について、意味を理解する。

  \section storm_replay_output_header header

\verbatim
{'m_dataBuildNum': 62212,
 'm_elapsedGameLoops': 16160,
 'm_ngdpRootKey': {},
 'm_signature': 'Heroes of the Storm replay\x1b11',
 'm_type': 2,
 'm_useScaledTime': False,
 'm_version': {'m_baseBuild': 62212,
               'm_build': 62212,
               'm_flags': 1,
               'm_major': 2,
               'm_minor': 30,
               'm_revision': 2}}
 \endverbatim

  - バージョン情報
  - m_elapsedGameLoops
    - フレーム数。16 で割った値がゲーム時間の秒数になる。
    - このデータでは 1010 秒であり、ゲーム時間は 16:50 となる。


  \section storm_replay_output_initdata initdata

  - ゲーム設定やヒーローの見た目などの情報が格納されている。
  - userId が定義されている。


  \section storm_replay_output_messageevents messageevents

  - Ping 情報、チャット情報が格納されている。
    - Draft 時のチャット情報は格納されていない。


  \section storm_replay_output_attributeevents attributeevents

  よくわからない。


  \section storm_replay_output_trackerevents trackerevents

  - BAN 情報
    - NNet.Replay.Tracker.SHeroBannedEvent
  - Lock in 情報
     - NNet.Replay.Tracker.SHeroPickedEvent

  - タレント選択情報
    - 'm_eventName': 'TalentChosen'
    - 'm_eventName': 'EndOfGameTalentChoices'

  - スキル
    - Zombie Wall などのユニットを配置するスキルについての情報も記録されている。
      - 'm_unitTypeName': 'WitchDoctorZombieWallUnit',

  - プレイヤーの Death 情報
    - 'm_eventName': 'PlayerDeath',

  - 経験値情報（定期的）
    - '_event': 'NNet.Replay.Tracker.SStatGameEvent',


  \section storm_replay_output_deails details

\verbatim
{'m_playerList': [{'m_color': {'m_a': 255, 'm_b': 255, 'm_g': 92, 'm_r': 36},
                   'm_control': 2,
                   'm_handicap': 0,
                   'm_hero': "Anub'arak",
                   'm_name': 'Ezla',
                   'm_observe': 0,
                   'm_race': ' ',
                   'm_result': 2,
                   'm_teamId': 0,
                   'm_toon': {'m_id': 1138283,
                              'm_programId': 'Hero',
                              'm_realm': 1,
                              'm_region': 1},
                   'm_workingSetSlotId': 0},
(中略)
                  {'m_color': {'m_a': 255, 'm_b': 0, 'm_g': 0, 'm_r': 255},
                   'm_control': 2,
                   'm_handicap': 0,
                   'm_hero': 'Nazeebo',
                   'm_name': 'Player',
                   'm_observe': 0,
                   'm_race': ' ',
                   'm_result': 1,
                   'm_teamId': 1,
                   'm_toon': {'m_id': 6289150,
                              'm_programId': 'Hero',
                              'm_realm': 1,
                              'm_region': 1},
                   'm_workingSetSlotId': 9}],
 'm_timeLocalOffset': 324000000000L,
 'm_timeUTC': 131632395403405802L,
 'm_title': 'Cursed Hollow'}
\endverbatim

  - m_playerList
    - m_color ... チームの色
    - m_hero ... ヒーロー名（クライアントの言語バージョンに依存したデータが格納される）
    - m_name ... プレイヤー名
    - m_observe ... 0: プレイヤー, 1: オブザーバー（なのかな？）
    - m_teamId ... チーム番号
    - m_result ... よくわからない
    - m_workingSetSlotId ... プレイヤーごとに振られたインデックス値

  - m_timeLocalOffset
    - UTC との時差を 100ナノ秒で表現したもの。
    - このゲームの 324000000000L の値は +9 時間になる。

  - m_timeUTC
    - リプレイファイルの作成時刻を 1970-01-01 からの100ナノ秒の経過時間で表したもの。
\code
# python での変換例
m_timeUTC = 131632395403405802
ts = datetime.datetime.fromtimestamp((m_timeUTC - 116444736000000000) // 10000000)
> 2018-02-16 16:25:40
\endcode
    - この時刻からゲーム時間を引くと、ゲーム開始時刻が得られる。（と思う）

  - m_title
    - マップ名、このデータでは 'Cursed Hollow'


  \section storm_replay_output_json json

  出力がなかった。


  \section storm_replay_output_gameevents gameevents

  -
*/
